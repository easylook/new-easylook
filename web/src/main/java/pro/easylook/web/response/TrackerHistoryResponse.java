package pro.easylook.web.response;

import pro.easylook.core.entity.TrackerDataH;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TrackerHistoryResponse {

    private double[] point;
    private Date logDate;

    private static final String DATE_FORMAT_PATTERN = "yyyy.MM.dd HH:mm:ss";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_PATTERN);

    private TrackerHistoryResponse() {}

    public static TrackerHistoryResponse fromTrackerData(TrackerDataH trackerDataH) {

        TrackerHistoryResponse response = new TrackerHistoryResponse();

        if (trackerDataH != null) {
            response.point = trackerDataH.getPoint();
            response.logDate = trackerDataH.getServerTime();
        }
        return response;
    }

    public double[] getPoint() {
        return point;
    }

    public String getLogDate() {
        return dateFormat.format(logDate);
    }
}
