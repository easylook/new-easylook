package pro.easylook.web.response;

import pro.easylook.core.entity.Group;
import pro.easylook.core.entity.Tracker;
import pro.easylook.core.entity.TrackerImage;

import java.util.List;
import java.util.stream.Collectors;


public class TrackerGroupResponse {

    private long id;

    private long userId;
    private String title;

    private String icon;

    private List<TrackerResponse> trackers;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<TrackerResponse> getTrackers() {
        return trackers;
    }

    public void setTrackers(List<TrackerResponse> trackers) {
        this.trackers = trackers;
    }

    public static TrackerGroupResponse fromGroup(Group trackerGroup) {
        final TrackerGroupResponse trackerGroupResponse = new TrackerGroupResponse();
        if (trackerGroup != null) {
            trackerGroupResponse.setId(trackerGroup.getId());
            trackerGroupResponse.setUserId(trackerGroup.getUser().getId());
            trackerGroupResponse.setTitle(trackerGroup.getName());

            final TrackerImage image = trackerGroup.getImage();
            if (image != null) {
                trackerGroupResponse.setIcon(image.getLink());
            }

            final List<Tracker> trackers = trackerGroup.getTrackers();
            if (trackers != null) {
                List<TrackerResponse> trackerResponses = trackers.stream()
                        .map(TrackerResponse::fromTracker)
                        .collect(Collectors.toList());
                trackerGroupResponse.setTrackers(trackerResponses);
            }
        }
        return trackerGroupResponse;
    }
}
