package pro.easylook.web.response;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;
import pro.easylook.core.entity.Area;

public class ZoneResponse {

    private int id;

    private int userId;

    private double[][] coordinates;

    private double radius;

    private int type;

    private String title;


    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double[][] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(double[][] coordinates) {
        this.coordinates = coordinates;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static ZoneResponse fromArea(Area area) {
        ZoneResponse zr = new ZoneResponse();
        zr.setId(area.getId());
        zr.setTitle(area.getName());
        zr.setUserId(area.getUser().getId());
        zr.setType(area.getType());
        Polygon p = (Polygon)area.getShape();
        if (area.getType() == Area.TYPE_CIRCLE) {
            double[][] center = {{p.getCentroid().getX(), p.getCentroid().getY()}};
            zr.setCoordinates(center);
            zr.setRadius(Math.ceil(p.getLength() / (2 * Math.PI)));
        } else {
            zr.setRadius(0);
            Coordinate[] geoCoordinates = p.getCoordinates();
            double[][] coordinates = new double[geoCoordinates.length][2];
            //without last coordinate
            for (int i = 0; i < geoCoordinates.length; i++) {
                coordinates[i] = new double[]{geoCoordinates[i].x, geoCoordinates[i].y};
            }

            zr.setCoordinates(coordinates);
        }

        return zr;
    }
}
