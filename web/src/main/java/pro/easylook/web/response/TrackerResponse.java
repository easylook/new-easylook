package pro.easylook.web.response;

import pro.easylook.core.entity.*;

import java.util.Date;

public class TrackerResponse {
    private int id;

    private int userId;
    private String title;
    private String imei;
    private String phone;
    private boolean isOn;
    private int modelId;

    private Date historyStart;
    private Date historyEnd;

    private String group;
    private String icon;
    private String tariff;
    private String trackerType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn(boolean isOn) {
        this.isOn = isOn;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public Date getHistoryStart() {
        return historyStart;
    }

    public void setHistoryStart(Date historyStart) {
        this.historyStart = historyStart;
    }

    public Date getHistoryEnd() {
        return historyEnd;
    }

    public void setHistoryEnd(Date historyEnd) {
        this.historyEnd = historyEnd;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    public void setType(String type) {
        trackerType = type;
    }

    public String getTrackerType() {
        return trackerType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public static TrackerResponse fromTracker(Tracker tracker) {
        TrackerResponse trackerResponse = new TrackerResponse();
        if (tracker != null) {
            trackerResponse.setId(tracker.getId());
            trackerResponse.setUserId(tracker.getUser().getId());
            trackerResponse.setTitle(tracker.getName());
            trackerResponse.setImei(tracker.getTrackerEquipment().getImei());
            trackerResponse.setOn(tracker.isActive());
            trackerResponse.setModelId(tracker.getTrackerEquipment().getModel().getId());
            trackerResponse.setPhone(tracker.getTrackerEquipment().getPhoneNumber());

            Group group = tracker.getGroup();
            if (group != null) {
                trackerResponse.setGroup(group.getName());
            }

            TrackerImage icon = tracker.getTrackerImage();
            if (icon != null) {
                trackerResponse.setIcon(icon.getLink());
            }

            Tariff tariff = tracker.getTariff();
            if (tariff != null) {
                trackerResponse.setTariff(tariff.getName());
            }

            TrackerType trackerType = tracker.getTrackerType();
            if (trackerType != null) {
                trackerResponse.setType(trackerType.getName());
            }
        }
        return trackerResponse;
    }
}
