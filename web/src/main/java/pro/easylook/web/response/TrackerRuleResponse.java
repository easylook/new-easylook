package pro.easylook.web.response;

import pro.easylook.core.entity.Area;
import pro.easylook.core.entity.Rule;
import pro.easylook.core.entity.Tracker;
import pro.easylook.core.entity.User;

public class TrackerRuleResponse {

    private int id;

    private String title;

    private int type;

    private int userId;

    private int zoneId;

    private int trackerId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getZoneId() {
        return zoneId;
    }

    public void setZoneId(int zoneId) {
        this.zoneId = zoneId;
    }

    public int getTrackerId() {
        return trackerId;
    }

    public void setTrackerId(int trackerId) {
        this.trackerId = trackerId;
    }

    public static TrackerRuleResponse fromTrackerRule(Rule trackerRule) {
        TrackerRuleResponse trackerRuleResponse = new TrackerRuleResponse();
        if (trackerRule != null) {
            trackerRuleResponse.setId(trackerRule.getId());
            trackerRuleResponse.setType(trackerRule.getRuleType().getId());
            trackerRuleResponse.setTitle(trackerRule.getName());

            User user = trackerRule.getUser();
            if (user != null) {
                trackerRuleResponse.setUserId(user.getId());
            }

            Area area = trackerRule.getArea();
            if (area != null) {
                trackerRuleResponse.setZoneId(area.getId());
            }

            Tracker tracker = trackerRule.getTracker();
            if (tracker != null) {
                trackerRuleResponse.setTrackerId(tracker.getId());
            }
        }

        return trackerRuleResponse;
    }
}
