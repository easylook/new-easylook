package pro.easylook.web.response;

import com.google.common.collect.Lists;
import pro.easylook.core.entity.Tracker;
import pro.easylook.core.entity.TrackerDataH;
import pro.easylook.core.entity.TrackerDataP;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class TrackerDataResponse {

    private long id;
    private double lat;
    private double lng;
    private long logDate;
    private double speed;
    private long trackerId;
    private String trackerStatus;
    private MapResponse params;

    private TrackerDataResponse () {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public long getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate.getTime();
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public long getTrackerId() {
        return trackerId;
    }

    public void setTrackerId(long trackerId) {
        this.trackerId = trackerId;
    }

    public String getTrackerStatus() {
        return trackerStatus;
    }

    public void setTrackerStatusCode(String trackerStatus) {
        this.trackerStatus = trackerStatus;
    }

    public MapResponse getParams() {
        return params;
    }

    public void setParams(MapResponse params) {
        this.params = params;
    }

    public static TrackerDataResponse fromTrackerDataH(TrackerDataH trackerDataH) {
        TrackerDataResponse trackerDataResponse = new TrackerDataResponse();
        if (trackerDataH != null) {
            trackerDataResponse.setId(trackerDataH.getId());
            trackerDataResponse.setLat(trackerDataH.getLat());
            trackerDataResponse.setLng(trackerDataH.getLng());
            trackerDataResponse.setLogDate(trackerDataH.getServerTime());
            trackerDataResponse.setSpeed(trackerDataH.getSpeed());
            trackerDataResponse.setTrackerStatusCode(trackerDataH.getStatusCode());
            List<TrackerDataP> trackerDataPList = trackerDataH.getTrackerDataParams();

            if (trackerDataPList != null && !trackerDataPList.isEmpty()) {
                MapResponse mapResponse = new MapResponse();
                mapResponse.put("params", Lists.newArrayList(trackerDataPList));
                trackerDataResponse.setParams(mapResponse);
            }

            Tracker tracker = trackerDataH.getTracker();
            if (tracker != null) {
                trackerDataResponse.setTrackerId(tracker.getId());
            }
        }
        return trackerDataResponse;
    }
}
