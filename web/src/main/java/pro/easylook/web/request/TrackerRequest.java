package pro.easylook.web.request;

import java.util.Date;

public class TrackerRequest {
    private int id;

    private int userId;
    private String title;
    private String imei;
    private String phone;
    private boolean isOn;
    private int modelId;

    private Date historyStart;
    private Date historyEnd;

    private int groupId;
    private int iconId;
    private int tariffId;
    private int typeId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn(boolean isOn) {
        this.isOn = isOn;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public Date getHistoryStart() {
        return historyStart;
    }

    public void setHistoryStart(Date historyStart) {
        this.historyStart = historyStart;
    }

    public Date getHistoryEnd() {
        return historyEnd;
    }

    public void setHistoryEnd(Date historyEnd) {
        this.historyEnd = historyEnd;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public int getTariffId() {
        return tariffId;
    }

    public void setTariffId(int tariffId) {
        this.tariffId = tariffId;
    }

    public int getTypeid() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
