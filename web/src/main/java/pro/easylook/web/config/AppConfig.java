package pro.easylook.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author aevseenko
 * @since 2/25/15
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"pro.easylook.web"})
public class AppConfig extends WebMvcConfigurerAdapter {

    public static final String USER_ID_LABEL = "userId";
    public static final String USER_LOGIN_LABEL = "userLogin";
    public static final String USER_NAME_LABEL = "userName";
    public static final String TEXT_JAVASCRIPT = "text/javascript";
    public static final String UTF_8 = "UTF-8";
}
