package pro.easylook.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pro.easylook.web.config.AppConfig;
import pro.easylook.web.request.TrackerRuleRequest;
import pro.easylook.web.request.ZoneRequest;
import pro.easylook.web.response.TrackerRuleResponse;
import pro.easylook.web.response.ZoneResponse;
import pro.easylook.web.service.TrackerRuleService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/tracker-rule")
public class TrackerRuleController {

    @Autowired
    private TrackerRuleService trackerRuleService;

    @RequestMapping(method = RequestMethod.GET)
    public List<TrackerRuleResponse> getTrackerGroups(HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerRuleService.getRules(userId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public TrackerRuleResponse getZone(@PathVariable int id, HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerRuleService.getRule(id, userId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public boolean deleteZone(@PathVariable int id, HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerRuleService.deleteRule(id, userId);
    }

    @RequestMapping(method = {RequestMethod.POST})
    public TrackerRuleResponse createZone(@RequestBody TrackerRuleRequest trackerRuleRequest, HttpServletRequest request) {
        trackerRuleRequest.setId(0);
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerRuleService.saveRule(trackerRuleRequest, userId);
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.PUT})
    public TrackerRuleResponse updateTracker(@PathVariable int id, @RequestBody TrackerRuleRequest trackerRuleRequest, HttpServletRequest request) {
        if (trackerRuleRequest.getId() == 0) {
            return null;
        }
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerRuleService.saveRule(trackerRuleRequest, userId);
    }
}
