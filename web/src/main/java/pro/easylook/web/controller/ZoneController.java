package pro.easylook.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pro.easylook.web.config.AppConfig;
import pro.easylook.web.request.ZoneRequest;
import pro.easylook.web.response.ZoneResponse;
import pro.easylook.web.service.ZoneService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/zone")
public class ZoneController {

    @Autowired
    private ZoneService zoneService;

    @RequestMapping(method = RequestMethod.GET)
    public List<ZoneResponse> getZones(HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return zoneService.getZones(userId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ZoneResponse getZone(@PathVariable Integer id, HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return zoneService.getZone(id, userId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public boolean deleteZone(@PathVariable Integer id, HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return zoneService.deleteZone(id, userId);
    }

    @RequestMapping(method = {RequestMethod.POST})
    public ZoneResponse createZone(@RequestBody ZoneRequest zoneRequest, HttpServletRequest request) {
        zoneRequest.setId(0);
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return zoneService.saveZone(zoneRequest, userId);
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.PUT})
    public ZoneResponse updateTracker(@PathVariable Long id, @RequestBody ZoneRequest zoneRequest, HttpServletRequest request) {
        if (zoneRequest.getId() == 0) {
            return null;
        }
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return zoneService.saveZone(zoneRequest, userId);
    }
}
