package pro.easylook.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pro.easylook.web.config.AppConfig;
import pro.easylook.web.response.MapResponse;
import pro.easylook.web.service.DefaultService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * @author aevseenko
 * @since 2/26/15
 */
@RestController
@RequestMapping("/")
public class DefaultController {

    @Autowired
    DefaultService defaultService;

    @RequestMapping("/groups-with-trackers")
    public MapResponse getGroupsWithTrackers(HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return defaultService.getGroupsWithTrackers(userId);
    }

    @RequestMapping("/tracker-icons")
    public MapResponse getTrackerIcons() {
        return defaultService.getTrackerIcons();
    }

    @RequestMapping("/tracker-types")
    public MapResponse getTrackerTypes() {
        return defaultService.getTrackerTypes();
    }

    @RequestMapping("/models")
    public MapResponse getModels() {
        return defaultService.getModels();
    }

    @RequestMapping("/tariffs")
    public MapResponse getTariffs() {
        return defaultService.getTariffs();
    }

    @RequestMapping("/periodic")
    public MapResponse getLastTrackerPoints(HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return defaultService.getLastTrackerPoints(userId);
    }

    @RequestMapping("/tracker-history")
    public MapResponse getTrackerHistory(
            @RequestParam int trackerId,
            @RequestParam("fromDate") long fromTimestamp,
            @RequestParam("toDate") long toTimestamp) {

        Date fromDate = new Date(fromTimestamp * 1000L);
        Date toDate = new Date(toTimestamp * 1000L);
        return defaultService.getTrackerHistory(trackerId, fromDate, toDate);
    }

    private static final String USER_JS_RESPONSE = "window.serverData = {userLogin: \"%s\",userId: %d, userName: \"%s\"};";
    @RequestMapping("/user")
    @ResponseBody
    public String getUserName(HttpServletRequest request, HttpServletResponse response) {
        String userLogin = String.valueOf(request.getAttribute(AppConfig.USER_LOGIN_LABEL));
        String userName = String.valueOf(request.getAttribute(AppConfig.USER_NAME_LABEL));
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        response.setContentType(AppConfig.TEXT_JAVASCRIPT);
        response.setCharacterEncoding(AppConfig.UTF_8);
        return String.format(USER_JS_RESPONSE, userLogin, userId, userName);
    }
}
