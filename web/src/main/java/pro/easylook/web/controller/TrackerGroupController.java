package pro.easylook.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pro.easylook.web.config.AppConfig;
import pro.easylook.web.request.TrackerGroupRequest;
import pro.easylook.web.response.TrackerGroupResponse;
import pro.easylook.web.service.TrackerGroupService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author aevseenko
 * @since 2/27/15
 */
@RestController
@RequestMapping("/tracker-group")
public class TrackerGroupController {

    @Autowired
    private TrackerGroupService trackerGroupService;

    @RequestMapping(method = RequestMethod.GET)
    public List<TrackerGroupResponse> getTrackerGroups(HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerGroupService.getTrackerGroups(userId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public TrackerGroupResponse getTrackerGroup(@PathVariable int id, HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerGroupService.getTrackerGroup(id, userId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public boolean deleteTrackerGroup(@PathVariable int id, HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerGroupService.deleteTrackerGroup(id, userId);
    }

    @RequestMapping(method = {RequestMethod.POST})
    public TrackerGroupResponse createTrackerGroup(@RequestBody TrackerGroupRequest trackerGroup, HttpServletRequest request) {
        trackerGroup.setId(0);
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerGroupService.saveTrackerGroup(trackerGroup, userId);
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.PUT})
    public TrackerGroupResponse updateTrackerGroup(@PathVariable Long id, @RequestBody TrackerGroupRequest trackerGroup,
                                                   HttpServletRequest request) {
        if (trackerGroup.getId() == 0) {
            return null;
        }
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerGroupService.saveTrackerGroup(trackerGroup, userId);
    }
}
