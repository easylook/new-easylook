package pro.easylook.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pro.easylook.web.config.AppConfig;
import pro.easylook.web.request.TrackerRequest;
import pro.easylook.web.response.TrackerResponse;
import pro.easylook.web.service.TrackerService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author aevseenko
 * @since 2/27/15
 */
@RestController
@RequestMapping("/tracker")
public class TrackerController {

    @Autowired
    private TrackerService trackerService;

    @RequestMapping(method = RequestMethod.GET)
    public List<TrackerResponse> getTrackers(HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerService.getTrackers(userId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public TrackerResponse getTracker(@PathVariable Integer id, HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerService.getTracker(id, userId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public boolean deleteTracker(@PathVariable Integer id, HttpServletRequest request) {
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerService.deleteTracker(id, userId);
    }

    @RequestMapping(method = {RequestMethod.POST})
    public TrackerResponse createTracker(@RequestBody TrackerRequest tracker, HttpServletRequest request) {
        tracker.setId(0);
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerService.saveTracker(tracker, userId);
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.PUT})
    public TrackerResponse updateTracker(@PathVariable Long id, @RequestBody TrackerRequest tracker, HttpServletRequest request) {
        if (tracker.getId() == 0) {
            return null;
        }
        int userId = (int) request.getAttribute(AppConfig.USER_ID_LABEL);
        return trackerService.saveTracker(tracker, userId);
    }
}
