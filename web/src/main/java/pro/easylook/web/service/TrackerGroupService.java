package pro.easylook.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pro.easylook.core.entity.Group;
import pro.easylook.core.entity.TrackerImage;
import pro.easylook.core.entity.User;
import pro.easylook.core.repository.GroupRepository;
import pro.easylook.core.repository.TrackerImageRepository;
import pro.easylook.core.repository.UserRepository;
import pro.easylook.web.request.TrackerGroupRequest;
import pro.easylook.web.response.TrackerGroupResponse;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TrackerGroupService {
    @Autowired
    private DefaultService defaultService;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private TrackerImageRepository trackerImageRepository;
    @Autowired
    private UserRepository userRepository;

    public TrackerGroupResponse getTrackerGroup(int id, int userId) {
        User user = userRepository.findOne(userId);
        Group trackerGroup = groupRepository.findByIdAndUser(id, user);
        return TrackerGroupResponse.fromGroup(trackerGroup);
    }

    public List<TrackerGroupResponse> getTrackerGroups(int userId) {
        User user = userRepository.findOne(userId);
        List<Group> trackerGroups = groupRepository.findByUser(user);
        return trackerGroups
                .stream()
                .map(TrackerGroupResponse::fromGroup)
                .collect(Collectors.toList());
    }

    public boolean deleteTrackerGroup(int id, int userId) {
        User user = userRepository.findOne(userId);
        Group trackerGroup = groupRepository.findByIdAndUser(id, user);
        if (trackerGroup != null) {
            try {
                defaultService.logAction(user, "удаление группы");
                groupRepository.delete(id);
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public TrackerGroupResponse saveTrackerGroup(TrackerGroupRequest trackerGroupRequest, int userId) {
        Group trackerGroup;

        if (userId == 0) {
            return null;
        }

        String message = "изменение группы";
        if (trackerGroupRequest.getId() != 0) {
            trackerGroup = groupRepository.findOne(trackerGroupRequest.getId());
            if (trackerGroup == null) {
                return null;
            }
        }
        else {
            trackerGroup = new Group();
            trackerGroup.setCreated(new Date());
            message = "добавление группы";
        }

        TrackerImage image = trackerImageRepository.findOne(trackerGroupRequest.getIconId());
        User user = userRepository.findOne(userId);

        trackerGroup.setName(trackerGroupRequest.getTitle());
        trackerGroup.setUser(user);
        trackerGroup.setImage(image);
        defaultService.logAction(user, message);
        return TrackerGroupResponse.fromGroup(groupRepository.save(trackerGroup));
    }
}
