package pro.easylook.web.service;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.easylook.core.entity.*;
import pro.easylook.core.repository.*;
import pro.easylook.web.response.MapResponse;
import pro.easylook.web.response.TrackerDataResponse;
import pro.easylook.web.response.TrackerGroupResponse;
import pro.easylook.web.response.TrackerHistoryResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author aevseenko
 * @since 2/26/15
 */
@Service
@Transactional
public class DefaultService {

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    TrackerRepository trackerRepository;

    @Autowired
    TrackerDataHRepository trackerDataHRepository;

    @Autowired
    TrackerImageRepository trackerImageRepository;

    @Autowired
    TariffRepository tariffRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TrackerStatusDataRepository trackerStatusDataRepository;

    @Autowired
    TrackerTypeRepository trackerTypeRepository;

    @Autowired
    EquipmentModelRepository equipmentModelRepository;

    @Autowired
    LogRepository logRepository;

    public MapResponse getGroupsWithTrackers(int userId) {
        MapResponse mapResponse = new MapResponse();

        User user = userRepository.findOne(userId);

        List<Group> trackerGroups = Lists.newArrayList(groupRepository.findByUser(user));
        List<TrackerGroupResponse> trackerGroupResponses = trackerGroups.stream()
                .map(TrackerGroupResponse::fromGroup)
                .collect(Collectors.toList());

        mapResponse.put("groups", trackerGroupResponses);
        return mapResponse;
    }

    public MapResponse getTrackerIcons() {
        MapResponse mapResponse = new MapResponse();
        mapResponse.put("icons", Lists.newArrayList(trackerImageRepository.findAll()));
        return mapResponse;
    }
    
    public MapResponse getTariffs() {
        MapResponse mapResponse = new MapResponse();
        mapResponse.put("tariffs", Lists.newArrayList(tariffRepository.findAll()));
        return mapResponse;
    }

    public MapResponse getLastTrackerPoints(int userId) {
        User user = userRepository.findOne(userId);
        List<Tracker> trackers = trackerRepository.findByUser(user);

        MapResponse mapResponse = new MapResponse();
        List<TrackerDataH> trackerDatas = trackerDataHRepository.findLatestTrackerPoints(trackers);
        List<TrackerDataResponse> trackerDataResponse = trackerDatas.stream()
                .map(TrackerDataResponse::fromTrackerDataH)
                .collect(Collectors.toList());
        mapResponse.put("periodic", trackerDataResponse);
        return mapResponse;
    }

    public MapResponse getTrackerHistory(int trackerId, Date fromDate, Date toDate) {

        List<TrackerHistoryResponse> histories = new ArrayList<>();
        Tracker tracker = trackerRepository.findOne(trackerId);
        if (tracker != null) {
            List<TrackerDataH> trackerDatas = trackerDataHRepository.findByTrackerAndServerTimeBetween(tracker, fromDate, toDate);
            trackerDatas
                    .stream()
                    .map(TrackerHistoryResponse::fromTrackerData)
                    .forEach(histories::add);
        }

        MapResponse mapResponse = new MapResponse();
        mapResponse.put("history", histories);
        return mapResponse;
    }

    public MapResponse getTrackerTypes() {
        MapResponse mapResponse = new MapResponse();
        mapResponse.put("trackerTypes", Lists.newArrayList(trackerTypeRepository.findAll()));
        return mapResponse;
    }

    public MapResponse getModels() {
        MapResponse mapResponse = new MapResponse();
        mapResponse.put("equipmentModels", Lists.newArrayList(equipmentModelRepository.findAll()));
        return mapResponse;
    }

    public void logAction(User user, String message) {
        if (user != null && message != null && message.length() > 0) {
            final Log log = new Log();
            log.setUser(user);
            log.setMessage(message);
            logRepository.save(log);
        }
    }
}
