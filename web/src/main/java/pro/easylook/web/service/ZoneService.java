package pro.easylook.web.service;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.util.GeometricShapeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.easylook.core.entity.Area;
import pro.easylook.core.entity.User;
import pro.easylook.core.repository.AreaRepository;
import pro.easylook.core.repository.UserRepository;
import pro.easylook.web.request.ZoneRequest;
import pro.easylook.web.response.ZoneResponse;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ZoneService {

    @Autowired
    private DefaultService defaultService;
    @Autowired
    private AreaRepository areaRepository;
    @Autowired
    private UserRepository userRepository;


    public List<ZoneResponse> getZones(int userId) {
        User user = userRepository.findOne(userId);
        List<Area> zones = areaRepository.findByUser(user);

        return zones
                .stream()
                .map(ZoneResponse::fromArea)
                .collect(Collectors.toList());
    }

    public ZoneResponse getZone(int id, int userId) {
        User user = userRepository.findOne(userId);
        Area zone = areaRepository.findByIdAndUser(id, user);
        return ZoneResponse.fromArea(zone);
    }

    public boolean deleteZone(int id, int userId) {
        User user = userRepository.findOne(userId);
        Area zone = areaRepository.findByIdAndUser(id, user);
        if (zone != null) {
            try {
                areaRepository.delete(id);
                defaultService.logAction(user, "удаление зоны");
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public ZoneResponse saveZone(ZoneRequest zoneRequest, int userId) {
        if (userId == 0) {
            return null;
        }

        Area area;
        if (zoneRequest.getId() != 0) {
            area = areaRepository.findOne(zoneRequest.getId());
            if (area == null) {
                return null;
            }
        }
        else {
            area = new Area();
        }

        bindZoneRequestToZone(zoneRequest, area);

        return ZoneResponse.fromArea(areaRepository.save(area));
    }

    private void bindZoneRequestToZone(ZoneRequest zoneRequest, Area area) {
        User user = userRepository.findOne(zoneRequest.getUserId());
        area.setUser(user);
        area.setType(zoneRequest.getType());
        area.setName(zoneRequest.getTitle());
        defaultService.logAction(user, "изменение зоны");
        if (zoneRequest.getType() == Area.TYPE_CIRCLE) {
            GeometricShapeFactory gsf = new GeometricShapeFactory();
            double [] center = zoneRequest.getCoordinates()[0];
            gsf.setCentre(new Coordinate(center[0], center[1]));
            gsf.setWidth(zoneRequest.getRadius() * 2);
            gsf.setHeight(zoneRequest.getRadius() * 2);
            area.setShape(gsf.createCircle());
        } else {
            GeometryFactory fact = new GeometryFactory();
            double [][] zrc = zoneRequest.getCoordinates();
            Coordinate [] coordinates = new Coordinate[zrc.length];

            for (int i = 0; i < zrc.length; i++) {
                coordinates[i] = new Coordinate(zrc[i][0], zrc[i][1]);
            }

            LinearRing linear = new GeometryFactory().createLinearRing(coordinates);
            area.setShape(new Polygon(linear, null, fact));
        }
    }
}
