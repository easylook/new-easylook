package pro.easylook.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.easylook.core.entity.*;
import pro.easylook.core.repository.*;
import pro.easylook.web.request.TrackerRequest;
import pro.easylook.web.response.TrackerResponse;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TrackerService {

    @Autowired
    private DefaultService defaultService;
    @Autowired
    private TrackerRepository trackerRepository;
    @Autowired
    private GroupRepository trackerGroupRepository;
    @Autowired
    private TrackerImageRepository trackerImageRepository;
    @Autowired
    private TariffRepository tariffRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TrackerTypeRepository trackerTypeRepository;
    @Autowired
    private EquipmentModelRepository equipmentModelRepository;
    @Autowired
    private TrackerEquipmentRepository trackerEquipmentRepository;

    public TrackerResponse getTracker(int id, int userId) {
        User user = userRepository.findOne(userId);
        Tracker tracker = trackerRepository.findByIdAndUser(id, user);
        return TrackerResponse.fromTracker(tracker);
    }

    public List<TrackerResponse> getTrackers(Integer userId) {
        User user = userRepository.findOne(userId);
        List<Tracker> trackers = trackerRepository.findByUser(user);
        return trackers
                .stream()
                .map(TrackerResponse::fromTracker)
                .collect(Collectors.toList());
    }

    public boolean deleteTracker(int id, int userId) {
        User user = userRepository.findOne(userId);
        Tracker tracker = trackerRepository.findByIdAndUser(id, user);
        if (tracker != null) {
            try {
                defaultService.logAction(user, "удаление трекера");
                trackerRepository.delete(id);
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public TrackerResponse saveTracker(TrackerRequest trackerRequest, int userId) {
        if (userId == 0) {
            return null;
        }

        Tracker tracker;
        TrackerEquipment trackerEquipment = null;
        String message = "изменение трекера";
        if (trackerRequest.getId() != 0) {
            tracker = trackerRepository.findOne(trackerRequest.getId());
            if (tracker == null) {
                return null;
            } else {
                trackerEquipment = tracker.getTrackerEquipment();
            }
        }
        else {
            tracker = new Tracker();
            tracker.setCreated(new Date());
            message = "добавление нового трекера";
        }

        if (trackerEquipment == null) {
            trackerEquipment = new TrackerEquipment();
            trackerEquipment.setCreated(new Date());
        }

        Group trackerGroup = trackerGroupRepository.findOne(trackerRequest.getGroupId());
        TrackerImage icon = trackerImageRepository.findOne(trackerRequest.getIconId());
        Tariff tariff = tariffRepository.findOne(trackerRequest.getTariffId());
        User user = userRepository.findOne(userId);
        EquipmentModel model = equipmentModelRepository.findOne(trackerRequest.getModelId());
        TrackerType trackerType = trackerTypeRepository.findOne(trackerRequest.getTypeid());

        tracker.setName(trackerRequest.getTitle());
        tracker.setUser(user);
        tracker.setActive(trackerRequest.isOn());

        if (tariff != null) {
            tracker.setTariff(tariff);
        }

        if (trackerGroup != null) {
            tracker.setGroup(trackerGroup);
        }

        if (icon != null) {
            tracker.setTrackerImage(icon);
        }

        if (trackerType != null) {
            tracker.setTrackerType(trackerType);
        }

        tracker = trackerRepository.save(tracker);
        trackerEquipment.setImei(trackerRequest.getImei());
        trackerEquipment.setPhoneNumber(trackerRequest.getPhone());
        trackerEquipment.setName(trackerRequest.getTitle());
        trackerEquipment.setTracker(tracker);
        if (model != null) {
            trackerEquipment.setModel(model);
        }
        tracker.setTrackerEquipment(trackerEquipment);
        trackerEquipmentRepository.save(trackerEquipment);
        defaultService.logAction(user, message);
        return TrackerResponse.fromTracker(tracker);
    }
}
