package pro.easylook.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.easylook.core.entity.*;
import pro.easylook.core.repository.*;
import pro.easylook.web.request.TrackerRuleRequest;
import pro.easylook.web.response.TrackerRuleResponse;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TrackerRuleService {

    @Autowired
    private DefaultService defaultService;

    @Autowired
    private RuleRepository trackerRuleRepository;

    @Autowired
    private TrackerRepository trackerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AreaRepository areaRepository;

    @Autowired
    private RuleTypeRepository ruleTypeRepository;


    public List<TrackerRuleResponse> getRules(int userId) {
        User user = userRepository.findOne(userId);
        List<Rule> rules = trackerRuleRepository.findByUser(user);

        return rules
                .stream()
                .map(TrackerRuleResponse::fromTrackerRule)
                .collect(Collectors.toList());
    }

    public TrackerRuleResponse getRule(int id, int userId) {
        User user = userRepository.findOne(userId);
        Rule trackerRule = trackerRuleRepository.findByIdAndUser(id, user);
        return TrackerRuleResponse.fromTrackerRule(trackerRule);
    }

    public boolean deleteRule(int id, int userId) {
        User user = userRepository.findOne(userId);
        Rule trackerRule = trackerRuleRepository.findByIdAndUser(id, user);
        if (trackerRule != null) {
            try {
                trackerRuleRepository.delete(id);
                defaultService.logAction(user, "удаление правила");
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public TrackerRuleResponse saveRule(TrackerRuleRequest trackerRuleRequest, long userId) {
        if (userId == 0) {
            return null;
        }

        Rule trackerRule;
        if (trackerRuleRequest.getId() != 0) {
            trackerRule = trackerRuleRepository.findOne(trackerRuleRequest.getId());
            if (trackerRule == null) {
                return null;
            }
        }
        else {
            trackerRule = new Rule();
        }

        bindTrackerRuleRequestToTrackerRule(trackerRuleRequest, trackerRule);

        return TrackerRuleResponse.fromTrackerRule(trackerRuleRepository.save(trackerRule));
    }

    private void bindTrackerRuleRequestToTrackerRule(TrackerRuleRequest trackerRuleRequest, Rule trackerRule) {
        trackerRule.setName(trackerRuleRequest.getTitle());
        RuleType ruleType = ruleTypeRepository.findOne(trackerRuleRequest.getType());
        User user = userRepository.findOne(trackerRuleRequest.getUserId());
        Tracker tracker = trackerRepository.findByIdAndUser(trackerRuleRequest.getTrackerId(), user);
        Area zone = areaRepository.findByIdAndUser(trackerRuleRequest.getZoneId(), user);

        if (user != null && tracker != null && zone != null && ruleType != null) {
            trackerRule.setUser(user);
            trackerRule.setArea(zone);
            trackerRule.setTracker(tracker);
            trackerRule.setRuleType(ruleType);
            defaultService.logAction(user, "изменение правила");
        }
    }
}
