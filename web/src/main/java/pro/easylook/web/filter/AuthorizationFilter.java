package pro.easylook.web.filter;

import net.spy.memcached.MemcachedClient;
import org.springframework.web.util.WebUtils;
import pro.easylook.web.config.AppConfig;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by andrey on 2/28/15.
 */
public class AuthorizationFilter implements Filter {

    private static final String MEMCACHED_REGEXP_ID = "id";
    private static final String MEMCACHED_REGEXP_LOGIN = "login";
    private static final String MEMCACHED_REGEXP_USERNAME = "username";
    private static final String MEMCACHED_REGEXP = "\"id\";s:\\d+:\"(?<id>\\d+)\".*?\"login\";s:\\d+:\"(?<login>\\w+)\".*?\"username\";s:\\d+:\"(?<username>.*?)\"";

    private static final String MEMCACHED_SESSION_PREFIX = "memc.sess.key.";
    private static final String JSESSIONID_COOKIE = "JSESSIONID";

    private static MemcachedClient memcachedClient;

    static {
        try {
            memcachedClient = new MemcachedClient(new InetSocketAddress("localhost", 11211));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Do nothing
    }

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        Cookie jSessionCookie = WebUtils.getCookie(httpRequest, JSESSIONID_COOKIE);

        if (jSessionCookie == null) {
            httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
        }

        String memcachedSessionId = MEMCACHED_SESSION_PREFIX + jSessionCookie.getValue();
        SessionUser sessionUser = getSessionUser(memcachedSessionId);

        if (sessionUser == null) {
            httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
        else {
            httpRequest.setAttribute(AppConfig.USER_ID_LABEL, sessionUser.getId());
            httpRequest.setAttribute(AppConfig.USER_LOGIN_LABEL, sessionUser.getLogin());
            httpRequest.setAttribute(AppConfig.USER_NAME_LABEL, sessionUser.getUsername());
        }

        chain.doFilter(httpRequest, httpResponse);
    }

    @Override
    public void destroy() {
        // Do nothing
    }

    public static SessionUser getSessionUser(String sessionID) {
        final Object valueObj = memcachedClient.get(sessionID);
        if (valueObj == null) {
            return null;
        }

        final String value = String.valueOf(valueObj);

        final Pattern p = Pattern.compile(MEMCACHED_REGEXP);
        final Matcher m = p.matcher(value);

        if (m.find()) {
            final int id = Integer.valueOf(m.group(MEMCACHED_REGEXP_ID));
            final String login = m.group(MEMCACHED_REGEXP_LOGIN);
            final String username = m.group(MEMCACHED_REGEXP_USERNAME);

            return new SessionUser(id, login, username);
        }

        return null;
    }

    private static class SessionUser {
        private int id;
        private final String login;
        private String username;

        public SessionUser(int id, String login, String username) {
            this.id = id;
            this.login = login;
            this.username = username;
        }

        public int getId() {
            return id;
        }

        public String getLogin() {
            return login;
        }

        public String getUsername() {
            return username;
        }

        @Override
        public String toString() {
            return "SessionUser#" + id + ":" + login + ":" + username;
        }
    }
}

