#!/usr/bin/env bash

service jetty stop

rm -f /etc/default/jetty
rm -f /etc/init.d/jetty
rm -f /etc/jetty
rm -f /var/log/jetty
rm -rf /opt/jetty

userdel jetty