#!/usr/bin/env bash

JETTY_VERSION="9.2.9.v20150224"

cd /tmp

wget http://mirror.tspu.ru/eclipse/jetty/stable-9/dist/jetty-distribution-${JETTY_VERSION}.tar.gz
tar -xzvf jetty-distribution-${JETTY_VERSION}.tar.gz
rm jetty-distribution-${JETTY_VERSION}.tar.gz
mv jetty-distribution-${JETTY_VERSION} /opt/jetty


echo "JETTY_HOME=/opt/jetty" > /etc/default/jetty
echo "JETTY_USER=jetty" >> /etc/default/jetty
echo "JETTY_LOGS=/opt/jetty/logs/" >> /etc/default/jetty

useradd jetty
chown -R jetty:jetty /opt/jetty

ln -s /opt/jetty/bin/jetty.sh /etc/init.d/jetty
ln -s /opt/jetty/start.ini /opt/jetty/etc/start.ini
ln -s /opt/jetty/etc /etc/jetty
ln -s /opt/jetty/logs /var/log/jetty

service jetty start