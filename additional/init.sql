INSERT INTO "pattern" ("id", "pattern") VALUES(1, '#(?<imei>[0-9]{15}),.*:(?<year>[0-9]{2})(?<month>[0-9]{2})(?<day>[0-9]{2}),TIME:(?<time>[0-9]{6}),LAT:(?<lat>[0-9]{2,3}\.[0-9]*)\w{0,1},LOT:(?<lng>[0-9]{2,3}\.[0-9]*).*,Speed:(?<speed>[0-9]{2,3}\.[0-9]*)');
INSERT INTO "pattern" ("id", "pattern") VALUES(2, '#(?<imei>[0-9]{15}).*#(?<lng>[0-9]{4,5}\.[0-9]{4}).*(?<lat>[0-9]{4,5}\.[0-9]{4}).*(?<speed>[0-9]{3}\.[0-9]{2}).*#(?<day>[0-9]{2})(?<month>[0-9]{2})(?<year>[0-9]{2})#(?<time>[0-9]{6})');


--
-- Dumping data for table "icon"
--

INSERT INTO "icon" ("id", "link") VALUES(2, '/images/tracker_icons/ico_track_2.png');
INSERT INTO "icon" ("id", "link") VALUES(3, '/images/tracker_icons/ico_track_3.png');
INSERT INTO "icon" ("id", "link") VALUES(4, '/images/tracker_icons/ico_track_4.png');
INSERT INTO "icon" ("id", "link") VALUES(5, '/images/tracker_icons/ico_track_5.png');
INSERT INTO "icon" ("id", "link") VALUES(6, '/images/tracker_icons/ico_track_6.png');
INSERT INTO "icon" ("id", "link") VALUES(7, '/images/tracker_icons/ico_track_7.png');
INSERT INTO "icon" ("id", "link") VALUES(8, '/images/tracker_icons/ico_track_8.png');
INSERT INTO "icon" ("id", "link") VALUES(9, '/images/tracker_icons/ico_track_9.png');
INSERT INTO "icon" ("id", "link") VALUES(10, '/images/tracker_icons/ico_track_10.png');
INSERT INTO "icon" ("id", "link") VALUES(11, '/images/tracker_icons/ico_track_11.png');
INSERT INTO "icon" ("id", "link") VALUES(13, '/images/tracker_icons/ico_track_13.png');
INSERT INTO "icon" ("id", "link") VALUES(14, '/images/tracker_icons/ico_track_14.png');

--
-- Dumping data for table "tariff"
--

INSERT INTO "tariff" ("id", "title") VALUES(1, 'Тариф 1');
INSERT INTO "tariff" ("id", "title") VALUES(2, 'Тариф 2');

--
-- Dumping data for table "trackergroup"
--

INSERT INTO "trackergroup" ("id", "userid", "title", "icon_id") VALUES(1, 1, 'Беловы', 11);
INSERT INTO "trackergroup" ("id", "userid", "title", "icon_id") VALUES(2, 1, 'Группа 2', 10);
INSERT INTO "trackergroup" ("id", "userid", "title", "icon_id") VALUES(4, 1, 'Новая группа', 2);
INSERT INTO "trackergroup" ("id", "userid", "title", "icon_id") VALUES(5, 8, 'Семья', 4);


--
-- Dumping data for table "tracker"
--

INSERT INTO "tracker" ("id", "group_id", "userid", "title", "imei", "ison", "modelid", "icon_id", "tariff_id", "historystart", "historyend") VALUES(8, 1, 1, 'Оле2', 353966050574396, true, 5, 8, 2, NULL, NULL);
INSERT INTO "tracker" ("id", "group_id", "userid", "title", "imei", "ison", "modelid", "icon_id", "tariff_id", "historystart", "historyend") VALUES(10, 4, 1, 'Personal Phone', 123456, true, 1, 4, 1, NULL, NULL);
INSERT INTO "tracker" ("id", "group_id", "userid", "title", "imei", "ison", "modelid", "icon_id", "tariff_id", "historystart", "historyend") VALUES(11, 5, 8, 'Ксюша', 358918051884642, true, 1, 8, 1, NULL, NULL);
INSERT INTO "tracker" ("id", "group_id", "userid", "title", "imei", "ison", "modelid", "icon_id", "tariff_id", "historystart", "historyend") VALUES(12, 4, 1, 'Blue watches', 356823031231082, true, 1, 5, 1, NULL, NULL);
INSERT INTO "tracker" ("id", "group_id", "userid", "title", "imei", "ison", "modelid", "icon_id", "tariff_id", "historystart", "historyend") VALUES(13, 1, 1, 'Анютка', 354705060362635, false, 1, 2, 1, NULL, NULL);
INSERT INTO "tracker" ("id", "group_id", "userid", "title", "imei", "ison", "modelid", "icon_id", "tariff_id", "historystart", "historyend") VALUES(14, 4, 1, 'Брелок TL201', 861785008349932, true, 1, 11, 1, NULL, NULL);
INSERT INTO "tracker" ("id", "group_id", "userid", "title", "imei", "ison", "modelid", "icon_id", "tariff_id", "historystart", "historyend") VALUES(15, 4, 1, 'Егор', 865593020056048, true, 1, 7, 1, NULL, NULL);
INSERT INTO "tracker" ("id", "group_id", "userid", "title", "imei", "ison", "modelid", "icon_id", "tariff_id", "historystart", "historyend") VALUES(16, 5, 8, 'Черный брелок', 861785008349932, true, 1, 11, 1, NULL, NULL);