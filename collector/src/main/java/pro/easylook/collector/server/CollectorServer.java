package pro.easylook.collector.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pro.easylook.collector.config.CollectorConfig;
import pro.easylook.collector.model.CollectorInputMessage;
import pro.easylook.collector.notification.EmaiMessageSender;
import pro.easylook.collector.parser.CollectorParser;
import pro.easylook.collector.replication.Replicator;
import pro.easylook.collector.service.DataService;
import pro.easylook.core.config.CoreConfig;
import pro.easylook.core.entity.Protocol;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created
 * by artem
 * on 6/26/15.
 */
public class CollectorServer  {

    public static final BlockingQueue<CollectorInputMessage> INPUT_QUEUE = new LinkedBlockingDeque<>();
    public static final BlockingQueue<EmaiMessageSender.EmailMessage> EMAIL_QUEUE = new LinkedBlockingDeque<>();
    public static final int PERIOD = 30 * 1000;
    private static final Logger LOG = LogManager.getLogger(CollectorServer.class);
    private static final String SERVER_BIND_PROTOCOL_ON_PORT = "server bind protocol {} on port {}";
    private static final String SERVER_STARTING_NUMBER_OF_PROTOCOLS = "server starting:  Number of protocols {}";
    private static final String SERVER_CREATED_BOSS_THREADS_WORKER_THREADS = "server created: bossThreads {} workerThreads {}";
    private static final int SO_BACKLOG_VALUE = 64;
    @Autowired
    private DataService dataService;

    private final String host;
    private final EventLoopGroup workerGroup;
    private final EventLoopGroup bossGroup;
    private final Timer replicatorTimer = new Timer(true);

    public CollectorServer(int bossGroupThreads, int workerGroupThreads, String collectorHost) {
        bossGroup = new NioEventLoopGroup(bossGroupThreads);
        workerGroup = new NioEventLoopGroup(workerGroupThreads);
        host = collectorHost;
        LOG.info(SERVER_CREATED_BOSS_THREADS_WORKER_THREADS, bossGroupThreads, workerGroupThreads);
    }

    public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(CollectorConfig.class);
        context.register(CoreConfig.class);
        context.refresh();
        final CollectorParser parser = context.getBean("collectorParser", CollectorParser.class);
        parser.start();
        final EmaiMessageSender emaiMessageSender = context.getBean("emaiMessageSender", EmaiMessageSender.class);
        emaiMessageSender.start();
        final CollectorServer server = context.getBean("collectorServer", CollectorServer.class);
        server.startReplicator();
        server.run();
    }

    private void run() {
        final List<Protocol> protocols = dataService.getAllProtocols();
        LOG.info(SERVER_STARTING_NUMBER_OF_PROTOCOLS, protocols.size());
        final List<ChannelFuture> channelFutureList = new ArrayList<>();
        try {
            for (Protocol protocol : protocols) {
                final ServerBootstrap b = new ServerBootstrap();
                b.group(bossGroup, workerGroup)
                        .channel(NioServerSocketChannel.class)
                        .childHandler(new ChannelInitializer<SocketChannel>() {
                            @Override
                            public void initChannel(SocketChannel ch) throws Exception {
                                ch.pipeline().addLast(new CollectorInboundHandler(protocol));
                            }
                        })
                        .option(ChannelOption.SO_BACKLOG, SO_BACKLOG_VALUE)
                        .childOption(ChannelOption.SO_KEEPALIVE, true);
                final ChannelFuture cf = b.bind(host, protocol.getPort());
                LOG.info(SERVER_BIND_PROTOCOL_ON_PORT, protocol.getName(), protocol.getPort());
                channelFutureList.add(cf);
            }
        } catch (Exception e) {
            LOG.error(e);
        } finally {
            try {
                for (ChannelFuture future : channelFutureList) {
                    if (future != null) {
                        future.sync().channel().closeFuture().sync();
                    }
                }
            } catch (InterruptedException e) {
                LOG.error(e);
            }
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    private void startReplicator() {
        replicatorTimer.scheduleAtFixedRate(new Replicator(dataService), 0, PERIOD);
    }
}
