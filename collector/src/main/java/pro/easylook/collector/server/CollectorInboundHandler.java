package pro.easylook.collector.server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.easylook.collector.model.CollectorInputMessage;
import pro.easylook.core.entity.Protocol;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created
 * by artem
 * on 6/27/15.
 */
public class CollectorInboundHandler extends ChannelInboundHandlerAdapter {

    private static final String BYTES_RECEIVED_IN_PROTOCOL_WITH_PROTOCOL_ID = "{} bytes received in protocol with protocolId {}";
    private static final String INVALID_DATA_IN_PROTOCOL_WITH_PROTOCOL_ID_DATA = "invalid data in protocol with protocolId {} data {}";
    private static final String VALID_DATA_RECEIVED_IN_PROTOCOL_WITH_PROTOCOL_ID_IMEI_SN_DATA = "valid data received in protocol with protocolId {} imei/sn {} data {}";
    private static final Logger LOG = LogManager.getLogger(CollectorInboundHandler.class);
    private static final String TRACKER_ID = "trackerId";
    private final Protocol protocol;
    private final Pattern pattern;

    public CollectorInboundHandler(Protocol protocol) {
        this.protocol = protocol;
        pattern = (protocol.getIdRegexp() != null) ? Pattern.compile(protocol.getIdRegexp()) : null;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        final ByteBuf in = (ByteBuf) msg;
        try {
            if (in.isReadable()) {
                int readableBytes = in.readableBytes();
                final byte[] buf = new byte[readableBytes];
                readableBytes = considerLineBrakes(in, readableBytes);
                LOG.info(BYTES_RECEIVED_IN_PROTOCOL_WITH_PROTOCOL_ID, readableBytes, protocol.getName());
                in.getBytes(0, buf);
                final String clientData = new String(buf);
                if (pattern == null) {
                    handleDirectIdPosition(in, readableBytes, clientData);
                } else {
                    final Matcher m = pattern.matcher(clientData);
                    if (m.find()) {
                        final String imeiData = m.group(TRACKER_ID);
                        addMessageToQueue(clientData, imeiData);
                    } else {
                        LOG.error(INVALID_DATA_IN_PROTOCOL_WITH_PROTOCOL_ID_DATA, protocol.getName(), clientData);
                    }
                }
            }
        } catch (Exception e) {
            LOG.error(e);
        }finally {
            ReferenceCountUtil.release(msg);
            ctx.disconnect();
        }
    }

    private void handleDirectIdPosition(ByteBuf in, int readableBytes, String clientData) {
        if (readableBytes < protocol.getImeiOffset() + protocol.getImeiCount()) {
            LOG.error(INVALID_DATA_IN_PROTOCOL_WITH_PROTOCOL_ID_DATA, protocol.getName(), clientData);
        } else {
            final byte[] imeiBuff = new byte[protocol.getImeiCount()];
            in.getBytes(protocol.getImeiOffset(), imeiBuff);
            final String imeiData = new String(imeiBuff);
            addMessageToQueue(clientData, imeiData);
        }
    }

    private void addMessageToQueue(String clientData, String imeiData) {
        LOG.info(VALID_DATA_RECEIVED_IN_PROTOCOL_WITH_PROTOCOL_ID_IMEI_SN_DATA,
                protocol.getName(), imeiData, clientData);
        CollectorServer.INPUT_QUEUE.add(
                new CollectorInputMessage(imeiData, clientData, protocol.getId(), protocol.getParserClassName()));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        LOG.error(cause);
        ctx.close();
    }

    private int considerLineBrakes(ByteBuf in, int readableBytes) {
        if (in.getByte(readableBytes - 1) == 13 || in.getByte(readableBytes - 1) == 10) {
            readableBytes -= 1;
        }
        if (in.getByte(readableBytes - 1) == 13 || in.getByte(readableBytes - 2) == 10) {
            readableBytes -= 1;
        }
        return readableBytes;
    }
}
