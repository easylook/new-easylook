package pro.easylook.collector.service;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pro.easylook.core.entity.*;
import pro.easylook.core.repository.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class DataService {

    @Autowired
    private TrackerStatusDataRepository trackerStatusDataRepository;

    @Autowired
    private WaUserRepository waUserRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TrackerEquipmentRepository trackerEquipmentRepository;

    @Autowired
    private TrackerDataHRepository trackerDataHRepository;

    @Autowired
    private TrackerDataPRepository trackerDataPRepository;

    @Autowired
    private TrackerStatusRepository trackerStatusRepository;

    @Autowired
    private ProtocolRepository protocolRepository;

    @Autowired
    private TemplateRepository templateRepository;

    @Resource(name="redisTemplate")
    private ValueOperations<String, String> currentTrackerData;

    private static Map<String, TrackerStatus> trackerStatusMap = new HashMap<>();

    public String getPreviousAndSetCurrentTrackerData(String imei, String data) {
        return currentTrackerData.getAndSet(imei, data);
    }

    public Map<Integer, User> getAllUsers() {
        final Map<Integer, User> map = new HashMap<>();
        Lists.newArrayList(userRepository.findAll())
                .parallelStream()
                .forEach((user -> map.put(user.getId(), user)));

        return map;
    }

    public List<WaUser> getAllWaUsers() {
        return Lists.newArrayList(waUserRepository.findAll());
    }

    public void saveUserList(List<User> usersForSave) {
        userRepository.save(usersForSave);
    }

    public TrackerEquipment getTrackerEquipmentByImei(String imei) {
        return trackerEquipmentRepository.findOneByImei(imei);
    }

    public TrackerEquipment getTrackerEquipmentBySn(String sn) {
        return trackerEquipmentRepository.findOneBySn(sn);
    }

    public TrackerDataH saveTrackerDataH(TrackerDataH trackerDataH) {
        return trackerDataHRepository.save(trackerDataH);
    }

    public void saveTrackerDataP(List<TrackerDataP> paramList) {
        trackerDataPRepository.save(paramList);
    }

    public TrackerStatusData getLatestTrackerStatus(Tracker tracker) {
        return trackerStatusDataRepository.findLatestTrackerStatusData(tracker);
    }

    public TrackerStatus getTrackerStatusByCode(String currentStatusCode) {
        if (trackerStatusMap.isEmpty()) {
            Lists.newArrayList(trackerStatusRepository.findAll())
                    .parallelStream()
                    .forEach((trackerStatus -> trackerStatusMap.put(trackerStatus.getCode(), trackerStatus)));
        }

        return trackerStatusMap.get(currentStatusCode);
    }

    public void saveTrackerStatusData(TrackerStatusData trackerStatusData) {
        trackerStatusDataRepository.save(trackerStatusData);
    }

    public List<Protocol> getAllProtocols() {
        return Lists.newArrayList(protocolRepository.findAll());
    }

    public Template getTemplateByTagAndModel(String tag, EquipmentModel equipmentModel) {
        return templateRepository.findOneByTagAndModel(tag, equipmentModel);
    }
}
