package pro.easylook.collector.model;

import java.io.Serializable;

/**
 * Created
 * by artem
 * on 6/26/15.
 */
public class CollectorInputMessage implements Serializable {

    /**
     * full imei or s/n
     */
    private String trackerId;

    /**
     * message from tracker
     */
    private String message;

    /**
     * id of protocol entity
     */
    private int protocolId;

    private String parserClassName;

    public CollectorInputMessage(String trackerId, String message, int protocolId, String parserClassName) {
        this.trackerId = trackerId;
        this.message = message;
        this.protocolId = protocolId;
        this.parserClassName = parserClassName;
    }

    public int getProtocolId() {
        return protocolId;
    }

    public String getMessage() {
        return message;
    }

    public String getTrackerId() {
        return trackerId;
    }

    public String getParserClassName() {
        return parserClassName;
    }
}
