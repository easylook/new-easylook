package pro.easylook.collector.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import pro.easylook.collector.notification.EmaiMessageSender;
import pro.easylook.collector.parser.CollectorParser;
import pro.easylook.collector.server.CollectorServer;

import javax.mail.Session;
import java.util.Properties;

@Configuration
@ComponentScan(basePackages = {"pro.easylook.collector.service", "pro.easylook.collector.parser", "pro.easylook.collector.notification"} )
@PropertySource("classpath:/collector.properties")
@PropertySource(value = "file:/home/easylook/collector.properties", ignoreResourceNotFound = true)
public class CollectorConfig {

    @Autowired
    Environment env;

    @Bean
    public CollectorParser collectorParser() {
        return new CollectorParser();
    }

    @Bean
    public EmaiMessageSender emaiMessageSender() {
        return new EmaiMessageSender();
    }

    @Bean
    public CollectorServer collectorServer() {
        final int bossGroupThreads = Integer.valueOf(env.getProperty("collector.bossThreads"));
        final int workerGroupThreads = Integer.valueOf(env.getProperty("collector.workerThreads"));
        final String collectorHost = env.getProperty("collector.host");
        return new CollectorServer(bossGroupThreads, workerGroupThreads, collectorHost);
    }

    @Bean
    public JedisConnectionFactory jedisConnFactory() {
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setUsePool(true);
        return jedisConnectionFactory;
    }

    @Bean
    public RedisTemplate redisTemplate() {
        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(jedisConnFactory());
        return redisTemplate;
    }

    @Bean
    public Session session() {
        Properties properties = new Properties();
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.host", env.getProperty("mail.host"));
        properties.put("mail.smtp.port", env.getProperty("mail.port"));
        properties.put("mail.smtp.username", env.getProperty("mail.username"));
        properties.put("mail.smtp.password", env.getProperty("mail.password"));
        return Session.getDefaultInstance(properties, null);
    }
}
