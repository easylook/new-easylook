package pro.easylook.collector.replication;

import pro.easylook.collector.service.DataService;
import pro.easylook.core.entity.User;
import pro.easylook.core.entity.WaUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

public class Replicator extends TimerTask {

    private DataService service;

    public Replicator(DataService service) {
        this.service = service;
    }

    @Override
    public void run() {
        final Map<Integer, User> users = service.getAllUsers();
        final List<WaUser> waUsers = service.getAllWaUsers();
        final List<User> usersForSave = new ArrayList<>();

        for (WaUser waUser : waUsers) {
            User user = users.get((int)waUser.getId());

            if (user == null) {
                user = new User();
                user.setId((int)waUser.getId());
            }

            user.setfName(waUser.getFirstname());
            user.setmName(waUser.getMiddlename());
            user.setsName(waUser.getLastname());
            user.setLogin(waUser.getLogin());
            user.setPassword(waUser.getPassword());

            if (!waUser.getEmails().isEmpty()) {
                user.setEmail(waUser.getEmails().get(0).getEmail());
            }

            usersForSave.add(user);
        }

        service.saveUserList(usersForSave);
    }
}
