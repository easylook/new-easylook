package pro.easylook.collector.parser;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.easylook.collector.geocalc.EarthCalc;
import pro.easylook.collector.geocalc.Point;
import pro.easylook.core.entity.Area;
import pro.easylook.core.entity.Tracker;
import pro.easylook.core.entity.TrackerStatus;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ParserUtils {
    private static final Logger LOG = LogManager.getLogger();
    private static final String DATE_PATTERN = "yyMMddHHmmss";
    private static final float KILOMETERS_FACTOR = 1.852f;
    private static final int IN_PLACE_MIN_DISTANCE = 30;

    public static float convertToKilometers(float value) {
        return value * KILOMETERS_FACTOR;
    }

    public static Date getDate(String year, String month, String day, String time) {
        try {
            final DateFormat formatter = new SimpleDateFormat(DATE_PATTERN);
            return formatter.parse(year + month + day + time);
        } catch (ParseException e) {
            LOG.error(e);
        }

        return new Date();
    }

    public static float convertCoordinateToMetric(float value) {
        float result = (int) (value / 100);
        result += (value - result * 100)/60;
        return result;
    }



    public static String calculateTrackerStatusCode(BaseProtocolParser.RedisTrackerData previous, BaseProtocolParser.RedisTrackerData current) {
        if (previous == null || current == null) {
            return TrackerStatus.CODE_IN_PLACE;
        }

        return (calculateDistance(previous, current) > IN_PLACE_MIN_DISTANCE) ?
                TrackerStatus.CODE_IN_MOTION : TrackerStatus.CODE_IN_PLACE;
    }

    public static double calculateDistance(BaseProtocolParser.RedisTrackerData previous, BaseProtocolParser.RedisTrackerData current) {
        if (previous == null || current == null) {
            return 0;
        }

        return EarthCalc.getDistance(
                new Point(previous.getLat(), previous.getLng()), new Point(current.getLat(), current.getLng()));
    }



    public static boolean isZoneContainsTracker(Geometry zone, double lat, double lng) {
        Coordinate coordinate = new Coordinate(lat, lng);
        GeometryFactory gf = new GeometryFactory();
        return zone.contains(gf.createPoint(coordinate));
    }


    public static String generateEnterMessageString(Tracker tracker, Area area) {
        return String.format("Ваш трекер %s вошел в зону %s\n", tracker.getName(), area.getName());
    }

    public static String generateLeaveMessageString(Tracker tracker, Area area) {
        return String.format("Ваш трекер %s покинул в зону %s\n", tracker.getName(), area.getName());
    }

    public static Date getDate(String dateTimeString, String dateTimePattern) {
        try {
            final DateFormat formatter = new SimpleDateFormat(dateTimePattern);
            return formatter.parse(dateTimeString);
        } catch (ParseException e) {
            LOG.error(e);
        }

        return new Date();
    }

    public static boolean textIsEmpty(String text) {
        return text == null || "".equals(text);
    }
}
