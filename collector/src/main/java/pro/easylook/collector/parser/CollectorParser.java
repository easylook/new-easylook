package pro.easylook.collector.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import pro.easylook.collector.model.CollectorInputMessage;
import pro.easylook.collector.server.CollectorServer;
import pro.easylook.collector.service.DataService;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created
 * by artem
 * on 6/27/15.
 */
public class CollectorParser extends Thread {

    private static final String START_PARSING_DATA = "start parsing data imei/sn {} protocolId {} data {}";
    private static final String COLLECTOR_PARSER_FINISHED = "Collector parser finish!";
    private static final String COLLECTOR_PARSER_STARTED = "Collector parser start!";
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private static final Logger LOG = LogManager.getLogger(CollectorParser.class);

    @Autowired
    private DataService dataService;

    @Override
    public void run() {
        LOG.info(COLLECTOR_PARSER_STARTED);
        try {
            while (!isInterrupted()) {
                CollectorInputMessage msg = CollectorServer.INPUT_QUEUE.take();
                runMessageParser(msg);
            }
        } catch (InterruptedException e) {
            LOG.error(e);
        }
        LOG.info(COLLECTOR_PARSER_FINISHED);
    }

    private void runMessageParser(CollectorInputMessage msg) {
        if (msg != null) {
            LOG.info(START_PARSING_DATA, msg.getTrackerId(), msg.getProtocolId(), msg.getMessage());
            try {
                final Class<?> aClass = Class.forName(msg.getParserClassName());
                //noinspection unchecked
                executor.submit(((Class<Runnable>) aClass).getConstructor(CollectorInputMessage.class, DataService.class)
                        .newInstance(msg, dataService));
            } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                LOG.error(e);
            }
        }
    }
}
