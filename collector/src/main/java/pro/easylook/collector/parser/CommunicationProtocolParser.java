package pro.easylook.collector.parser;

import pro.easylook.collector.model.CollectorInputMessage;
import pro.easylook.collector.service.DataService;
import pro.easylook.core.entity.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created
 * by artem
 * on 7/2/15.
 */
public class CommunicationProtocolParser extends BaseProtocolParser {

    private static final String TAG = "V1";
    private static final String LOG_DATA = "DATA PARSED: imei:{} time:{} date:{} validFlag:{} lat:{} lng:{} speed:{} direction:{} phone:{} mode:{} hasGpsSignal:{} satCount:{} altitude:{} voltage:{}";
    private static final String BATTERY_VOLTAGE = "BATTERY_VOLTAGE";
    private static final String ALTITUDE = "ALTITUDE";
    private static final String SATELLITE_COUNT = "SATELLITE_COUNT";
    private static final String HAS_GPS_SIGNAL = "HAS_GPS_SIGNAL";
    private static final String MODE = "MODE";
    private static final String PHONE = "PHONE";
    private static final String VALID_FLAG = "VALID_FLAG";

    public CommunicationProtocolParser(CollectorInputMessage msg, DataService dataService) {
        super(msg, dataService);
    }

    @Override
    public void run() {
        final TrackerEquipment trackerEquipment = dataService.getTrackerEquipmentByImei(msg.getTrackerId());
        if (trackerEquipment != null) {
            final EquipmentModel equipmentModel = trackerEquipment.getModel();
            final Template template = dataService.getTemplateByTagAndModel(TAG, equipmentModel);
            if (template != null) {
                final TrackerMessage currentMessage = parseMessage(template.getPattern());
                if (currentMessage != null) {
                    bindDataToDb(currentMessage, trackerEquipment);
                }
            }
        }
    }

    private void bindDataToDb(TrackerMessage currentMessage, TrackerEquipment trackerEquipment) {
        final TrackerDataH trackerDataH = dataService.saveTrackerDataH(
                createAndBindTrackerDataH(trackerEquipment.getTracker(), currentMessage));
        final Map<String, TrackerParam> trackerParamMap = trackerEquipment.getModel().getTrackerParamsMap();
        final List<TrackerDataP> paramList = new ArrayList<>();
        paramList.add(createParam(trackerDataH, trackerParamMap.get(VALID_FLAG), currentMessage.isValid));
        paramList.add(createParam(trackerDataH, trackerParamMap.get(PHONE), currentMessage.phone));
        paramList.add(createParam(trackerDataH, trackerParamMap.get(MODE), currentMessage.mode));
        paramList.add(createParam(trackerDataH, trackerParamMap.get(HAS_GPS_SIGNAL), currentMessage.hasGpsSignal));
        paramList.add(createParam(trackerDataH, trackerParamMap.get(SATELLITE_COUNT), currentMessage.satelliteCount));
        paramList.add(createParam(trackerDataH, trackerParamMap.get(ALTITUDE), currentMessage.altitude));
        paramList.add(createParam(trackerDataH, trackerParamMap.get(BATTERY_VOLTAGE), currentMessage.batteryVoltage));
        dataService.saveTrackerDataP(paramList);
        bindStatus(trackerEquipment, trackerDataH);
    }

    private TrackerMessage parseMessage(String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(msg.getMessage());
        if (m.find()) {
            try {
                final String phone = m.group("phone");
                final String time = m.group("time");
                final String validFlag = m.group("validFlag");
                final String lat = m.group("lat");
                final String latDir = m.group("latDir");
                final String lng = m.group("lng");
                final String lngDir = m.group("lngDir");
                final String speed = m.group("speed");
                final String direction = m.group("direction");
                final String date = m.group("date");
                final String mode = m.group("mode");
                final String hasGpsSignal = m.group("hasGpsSignal");
                final String satelliteCount = m.group("satelliteCount");
                final String altitude = m.group("altitude");
                final String voltage = m.group("voltage");

                LOG.info(
                        LOG_DATA,
                        msg.getTrackerId(),
                        time,
                        date,
                        validFlag,
                        lat,
                        lng,
                        speed,
                        direction,
                        phone,
                        mode,
                        hasGpsSignal,
                        satelliteCount,
                        altitude,
                        voltage
                );

                return new TrackerMessage(
                        time,
                        date,
                        validFlag,
                        lat,
                        lng,
                        speed,
                        direction,
                        phone,
                        mode,
                        hasGpsSignal,
                        satelliteCount,
                        altitude,
                        voltage
                );
            } catch (IllegalStateException | IllegalArgumentException e) {
                LOG.error(e);
            }

        }
        return null;
    }

    private static final class TrackerMessage extends BaseProtocolParser.BaseTrackerMessage {
        private static final String VALID_FLAG = "A";
        private static final String DATE_TIME_PATTERN = "HHmmssddMMyy";
        private static final String AUTONOMOUS_CODE = "A";
        private static final String AUTONOMOUS = "autonomous";
        private static final String DGPS = "DGPS";
        private static final String DR = "DR";
        private static final String DR_CODE = "E";
        private static final String DGPS_CODE = "D";
        public final boolean isValid;
        public final String phone;
        public final String mode;
        public final boolean hasGpsSignal;
        public final int satelliteCount;
        public final float altitude;
        public final float batteryVoltage;

        public TrackerMessage(
                String time,
                String date,
                String validFlag,
                String lat,
                String lng,
                String speed,
                String direction,
                String phone,
                String mode,
                String hasGpsSignal,
                String satelliteCount,
                String altitude,
                String batteryVoltage
        ) {
            super(ParserUtils.getDate(time + date, DATE_TIME_PATTERN),
                    ParserUtils.convertCoordinateToMetric(Float.valueOf(lat)),
                    ParserUtils.convertCoordinateToMetric(Float.valueOf(lng)),
                    ParserUtils.textIsEmpty(speed) ? 0 : Float.valueOf(speed),
                    ParserUtils.textIsEmpty(direction) ? 0 : Integer.valueOf(direction));
            this.isValid = VALID_FLAG.equals(validFlag);
            this.phone = phone;
            this.mode = getModeString(mode);
            this.hasGpsSignal = "F".equals(hasGpsSignal);
            this.satelliteCount = Integer.valueOf(satelliteCount);
            this.altitude = altitude == null ? 0 : Float.valueOf(altitude);
            this.batteryVoltage = Float.valueOf(batteryVoltage);
        }

        private String getModeString(String mode) {
            if (AUTONOMOUS_CODE.equals(mode)) {
                return AUTONOMOUS;
            } else if (DGPS_CODE.equals(mode)) {
                return DGPS;
            } else if (DR_CODE.equals(mode)) {
                return DR;
            }

            return null;
        }
    }
}
