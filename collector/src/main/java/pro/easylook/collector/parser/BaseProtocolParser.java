package pro.easylook.collector.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pro.easylook.collector.model.CollectorInputMessage;
import pro.easylook.collector.notification.EmaiMessageSender;
import pro.easylook.collector.server.CollectorServer;
import pro.easylook.collector.service.DataService;
import pro.easylook.core.entity.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created
 * by artem
 * on 7/1/15.
 */
public abstract class BaseProtocolParser implements Runnable {

    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private static final String UNTIL_LAST_DATE = "2030-12-31";
    protected static final Logger LOG = LogManager.getLogger();

    protected DataService dataService;
    protected CollectorInputMessage msg;

    public BaseProtocolParser(CollectorInputMessage msg, DataService dataService) {
        this.msg = msg;
        this.dataService = dataService;
    }

    protected TrackerDataH createAndBindTrackerDataH(Tracker tracker, BaseTrackerMessage msg) {
        final TrackerDataH trackerDataH = new TrackerDataH();
        trackerDataH.setTracker(tracker);
        trackerDataH.setServerTime(new Date());
        trackerDataH.setTrackerTime(msg.messageDateTime);
        trackerDataH.setLat(msg.lat);
        trackerDataH.setLng(msg.lng);
        trackerDataH.setSpeed(msg.speed);
        trackerDataH.setDirection(msg.direction);

        return trackerDataH;
    }

    protected TrackerDataP createParam(TrackerDataH trackerDataH, TrackerParam trackerParam, Object value) {
        final TrackerDataP trackerDataP = new TrackerDataP();
        trackerDataP.setTrackerDataH(trackerDataH);
        trackerDataP.setTrackerParam(trackerParam);
        trackerDataP.setValue(String.valueOf(value));
        return  trackerDataP;
    }

    protected void bindStatus(TrackerEquipment trackerEquipment, TrackerDataH trackerDataH) {
        RedisTrackerData redisTrackerData = new RedisTrackerData(trackerDataH.getLat(), trackerDataH.getLng());
        final RedisTrackerData previousTrackerData = getPreviousAndSetCurrent(redisTrackerData, trackerEquipment.getImei());
        calculateAndSetTrackerStatus(redisTrackerData, previousTrackerData, trackerEquipment.getTracker(), trackerDataH);
        generateEventMessages(redisTrackerData, previousTrackerData, trackerEquipment.getTracker());
    }

    private RedisTrackerData getPreviousAndSetCurrent(RedisTrackerData trackerData, String imei) {
        String data = dataService.getPreviousAndSetCurrentTrackerData(imei, trackerData.toString());
        if (data != null) {
            return RedisTrackerData.fromString(data);
        }

        return null;
    }

    private void calculateAndSetTrackerStatus(RedisTrackerData trackerData, RedisTrackerData previous, Tracker tracker, TrackerDataH trackerDataH) {
        String currentStatusCode = TrackerStatus.CODE_IN_PLACE;
        if (previous != null) {
            currentStatusCode = ParserUtils.calculateTrackerStatusCode(previous, trackerData);
            LOG.info("Previous data {}", previous.toString());
        }

        final TrackerStatusData serverTrackerStatusData = dataService.getLatestTrackerStatus(tracker);

        if (serverTrackerStatusData == null) {
            addNewTrackerStatusData(tracker, currentStatusCode);
        } else if (!currentStatusCode.equals(serverTrackerStatusData.getTrackerStatus().getCode())) {
            serverTrackerStatusData.setUntil(new Date());
            dataService.saveTrackerStatusData(serverTrackerStatusData);
            addNewTrackerStatusData(tracker, currentStatusCode);
        }
        trackerDataH.setStatusCode(currentStatusCode);
        dataService.saveTrackerDataH(trackerDataH);
    }

    private void generateEventMessages(RedisTrackerData current, RedisTrackerData previous, Tracker tracker) {

        //TODO: don't forget add checking for channel
        final List<Rule> rules = tracker.getRules();
        if (previous == null || rules == null || rules.isEmpty()) {
            return;
        }

        for(Rule rule : rules) {
            boolean previousContains =
                    ParserUtils
                            .isZoneContainsTracker(rule.getArea().getShape(), previous.getLat(), previous.getLng());
            boolean currentContains =
                    ParserUtils
                            .isZoneContainsTracker(rule.getArea().getShape(), current.getLat(), current.getLng());
            if (previousContains != currentContains) {
                if (currentContains && RuleType.CODE_TYPE_ENTER.equals(rule.getRuleType().getCode())) {
                    sendMessage(
                            ParserUtils.generateEnterMessageString(tracker, rule.getArea()),
                            rule.getUser().getEmail());
                } else if (RuleType.CODE_TYPE_LEAVE.equals(rule.getRuleType().getCode())) {
                    sendMessage(
                            ParserUtils.generateLeaveMessageString(tracker, rule.getArea()),
                            rule.getUser().getEmail());
                }
            }
        }
    }

    private void sendMessage(String messageText, String email) {
        CollectorServer.EMAIL_QUEUE.add(
                new EmaiMessageSender.EmailMessage(email, messageText)
        );
    }

    private void addNewTrackerStatusData(Tracker tracker, String currentStatusCode) {
        final TrackerStatusData trackerStatusData = new TrackerStatusData();
        trackerStatusData.setTracker(tracker);
        trackerStatusData.setSince(new Date());
        DateFormat format = new SimpleDateFormat(DATE_PATTERN);
        try {
            trackerStatusData.setUntil(format.parse(UNTIL_LAST_DATE));
        } catch (ParseException e) {
            trackerStatusData.setUntil(new Date());
            LOG.error(e);
        }
        trackerStatusData.setTrackerStatus(dataService.getTrackerStatusByCode(currentStatusCode));
        dataService.saveTrackerStatusData(trackerStatusData);
    }

    protected static class BaseTrackerMessage {
        public final Date messageDateTime;
        public final float lat;
        public final float lng;
        public final float speed;
        public final int direction;

        public BaseTrackerMessage(Date messageDateTime, float lat, float lng, float speed, int direction) {
            this.messageDateTime = messageDateTime;
            this.lat = lat;
            this.lng = lng;
            this.speed = speed;
            this.direction = direction;
        }
    }

    static class RedisTrackerData {
        private static final String DIVIDER = ";";
        private float lat;
        private float lng;

        public RedisTrackerData(float lat, float lng) {
            this.lat = lat;
            this.lng = lng;
        }

        public float getLat() {
            return lat;
        }

        public float getLng() {
            return lng;
        }

        public String toString() {
            return String.valueOf(lat) + DIVIDER + String.valueOf(lng);
        }

        public static RedisTrackerData fromString(String value) {
            if (value != null) {
                final String[] splitData = value.split(DIVIDER);
                if (splitData.length == 2) {
                    return new RedisTrackerData(
                            Float.valueOf(splitData[0]),
                            Float.valueOf(splitData[1]));
                }
            }

            return null;
        }
    }
}
