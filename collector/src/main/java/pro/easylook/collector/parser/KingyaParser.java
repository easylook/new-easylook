package pro.easylook.collector.parser;

import pro.easylook.collector.model.CollectorInputMessage;
import pro.easylook.collector.service.DataService;
import pro.easylook.core.entity.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created
 * by artem
 * on 6/28/15.
 */
public class KingyaParser extends BaseProtocolParser {
    private static final String DATA_LOG = "DATA PARSED: sn:{} information type:{} time:{} validFlag:{} lat:{}{} lng:{}{} speed:{} direction:{} date:{} status:{}";
    private static final String SOS_CODE = "SOS";
    private static final String INFORMATION_VALID_CODE = "INFORMATION_VALID";
    private static final String LOW_BATTERY_CODE = "LOW_BATTERY";

    public KingyaParser(CollectorInputMessage msg, DataService dataService) {
        super(msg, dataService);
    }

    @Override
    public void run() {
        final TrackerEquipment trackerEquipment = dataService.getTrackerEquipmentBySn(msg.getTrackerId());
        final String tag = msg.getMessage().substring(15, 17);
        if (trackerEquipment != null) {
            final EquipmentModel equipmentModel = trackerEquipment.getModel();
            final Template template = dataService.getTemplateByTagAndModel(tag, equipmentModel);
            if (template != null) {
                final TrackerMessage currentMessage = parseMessage(template.getPattern());
                if (currentMessage != null) {
                    bindDataToDb(currentMessage, trackerEquipment);
                }
            }
        }
    }

    private void bindDataToDb(TrackerMessage currentMessage, TrackerEquipment trackerEquipment) {
        final TrackerDataH trackerDataH = dataService.saveTrackerDataH(
                createAndBindTrackerDataH(trackerEquipment.getTracker(), currentMessage));
        final Map<String, TrackerParam> trackerParamMap = trackerEquipment.getModel().getTrackerParamsMap();
        final List<TrackerDataP> paramList = new ArrayList<>();
        paramList.add(createParam(trackerDataH, trackerParamMap.get(INFORMATION_VALID_CODE), currentMessage.isValid));
        paramList.add(createParam(trackerDataH, trackerParamMap.get(SOS_CODE), currentMessage.isAlarm()));
        paramList.add(createParam(trackerDataH, trackerParamMap.get(LOW_BATTERY_CODE), currentMessage.isLowBattery()));
        dataService.saveTrackerDataP(paramList);
        bindStatus(trackerEquipment, trackerDataH);
    }

    private TrackerMessage parseMessage(String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(msg.getMessage());
        if (m.find()) {
            try {
                final String sn = m.group("sn");
                final String informationType = m.group("informationType");
                final String time = m.group("time");
                final String validFlag = m.group("validFlag");
                final String lat = m.group("lat");
                final String latDir = m.group("latDir");
                final String lng = m.group("lng");
                final String lngDir = m.group("lngDir");
                final String speed = m.group("speed");
                final String direction = m.group("direction");
                final String date = m.group("date");
                final String status = m.group("status");

                LOG.info(
                        DATA_LOG,
                        sn,
                        informationType,
                        time,
                        validFlag,
                        lat,
                        latDir,
                        lng,
                        lngDir,
                        speed,
                        direction,
                        date,
                        status
                );

                return new TrackerMessage(
                        time,
                        date,
                        validFlag,
                        lat,
                        lng,
                        speed,
                        direction,
                        status
                );
            } catch (IllegalStateException | IllegalArgumentException e) {
                LOG.error(e);
            }

        }
        return null;
    }

    private static final class TrackerMessage extends BaseProtocolParser.BaseTrackerMessage {
        private static final String VALID_FLAG = "A";
        private static final String DATE_TIME_PATTERN = "HHmmssddMMyy";
        private static final int LOW_BATTERY = 0x00200000;
        private static final int ALARM = 0x00002000;
        public final boolean isValid;
        public final long status;

        public TrackerMessage(
                String time,
                String date,
                String validFlag,
                String lat,
                String lng,
                String speed,
                String direction,
                String status
        ) {
            super(ParserUtils.getDate(time + date, DATE_TIME_PATTERN),
                    ParserUtils.convertCoordinateToMetric(Float.valueOf(lat)),
                    ParserUtils.convertCoordinateToMetric(Float.valueOf(lng)),
                    ParserUtils.textIsEmpty(speed) ? 0 : Float.valueOf(speed),
                    ParserUtils.textIsEmpty(direction) ? 0 : Integer.valueOf(direction));
            this.isValid = VALID_FLAG.equals(validFlag);
            this.status = Long.valueOf(status, 16);
        }

        public boolean isLowBattery() {
            return (status & LOW_BATTERY) != 0;
        }

        public boolean isAlarm() {
            return (status & ALARM) != 0;
        }
    }
}
