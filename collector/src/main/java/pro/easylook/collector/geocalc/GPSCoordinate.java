package pro.easylook.collector.geocalc;

public class GPSCoordinate extends DMSCoordinate {

    public GPSCoordinate(double wholeDegrees, double minutes) {
        super(wholeDegrees, minutes, 0);
    }
}
