package pro.easylook.collector.geocalc;

public class RadianCoordinate extends Coordinate {

    double radians;

    public RadianCoordinate(double radians) {
        this.decimalDegrees = Math.toDegrees(radians);
        this.radians = radians;
    }

    public double getRadians() {
        return radians;
    }
}
