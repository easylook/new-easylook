package pro.easylook.collector.notification;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import pro.easylook.collector.server.CollectorServer;

/**
 * Created
 * by artem
 * on 7/1/15.
 */
public class EmaiMessageSender extends Thread {

    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private EmailMessageComponent emailMessage;

    @Override
    public void run() {
        try {
            while (!isInterrupted()) {
                final EmailMessage msg = CollectorServer.EMAIL_QUEUE.take();
                if (msg != null) {
                    sendMessage(msg);
                }
            }
        } catch (InterruptedException e) {
            LOG.error(e);
        }
    }

    private void sendMessage(EmailMessage msg) {
        emailMessage
                .setTo(msg.addressTo)
                .setSubject(msg.message)
                .setText(msg.message)
                .send();
    }

    public static class EmailMessage {
        private String addressTo;
        private String message;

        public EmailMessage(String addressTo, String message) {
            this.addressTo = addressTo;
            this.message = message;
        }
    }
}
