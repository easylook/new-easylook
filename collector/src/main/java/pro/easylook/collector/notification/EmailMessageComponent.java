package pro.easylook.collector.notification;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Component
public class EmailMessageComponent {

    public static final String FROM_ADDRESS = "noreply@easylook.pro";
    private static final Logger LOG = LogManager.getLogger();

    @Autowired
    private Session session;
    private String toEmailAddress;
    private String subject;
    private String text;

    public EmailMessageComponent setTo(String toEmailAddress) {
        this.toEmailAddress = toEmailAddress;
        return this;
    }

    public EmailMessageComponent setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public EmailMessageComponent setText(String text) {
        this.text = text;
        return this;
    }

    public void send() {
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(FROM_ADDRESS));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmailAddress));

            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
            LOG.info(String.format("Mail to %s sent.", toEmailAddress));
        }
        catch (MessagingException e) {
            LOG.error(e);
        }
    }
}
