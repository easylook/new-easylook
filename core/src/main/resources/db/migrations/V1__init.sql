--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: easylook
--



--
-- Data for Name: t_user; Type: TABLE DATA; Schema: public; Owner: easylook
--

INSERT INTO t_user (fk_id, fv_fname, fv_mname, fv_sname, fv_login, fv_psw, fv_email) VALUES (1, 'Олег', '', 'Белов', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'crazyxoma@gmail.com');
INSERT INTO t_user (fk_id, fv_fname, fv_mname, fv_sname, fv_login, fv_psw, fv_email) VALUES (6, 'Хомяк', '', 'Хомячелло', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'xoma@list.ru');
INSERT INTO t_user (fk_id, fv_fname, fv_mname, fv_sname, fv_login, fv_psw, fv_email) VALUES (8, 'Ivan', '', 'Pakhomov', NULL, '4bed48d7e723c39aec0b530d3da224d8', 'verefail@gmail.com');
INSERT INTO t_user (fk_id, fv_fname, fv_mname, fv_sname, fv_login, fv_psw, fv_email) VALUES (9, 'Artem', '', 'Artemov', NULL, 'f399c1f9f092dbda68ec70ec0b15dfff', 'temamochalov@yandex.ru');


--
-- Data for Name: t_area; Type: TABLE DATA; Schema: public; Owner: easylook
--



--
-- Name: t_area_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_area_fk_id_seq', 1, false);


--
-- Data for Name: t_channel; Type: TABLE DATA; Schema: public; Owner: easylook
--



--
-- Data for Name: t_pattern; Type: TABLE DATA; Schema: public; Owner: easylook
--

INSERT INTO t_pattern (fk_id, fv_pattern, fv_description, fv_is_metric) VALUES (1, '#(?<imei>[0-9]{15}),.*:(?<year>[0-9]{2})(?<month>[0-9]{2})(?<day>[0-9]{2}),TIME:(?<time>[0-9]{6}),LAT:(?<lat>[0-9]{2,3}\.[0-9]*)\w{0,1},LOT:(?<lng>[0-9]{2,3}\.[0-9]*).*,Speed:(?<speed>[0-9]{2,3}\.[0-9]*),.*?,(?<direction>[0-9]{1,3})', NULL, true);
INSERT INTO t_pattern (fk_id, fv_pattern, fv_description, fv_is_metric) VALUES (2, '#(?<imei>[0-9]{15}).*#(?<lng>[0-9]{4,5}\.[0-9]{4}).*(?<lat>[0-9]{4,5}\.[0-9]{4}).*(?<speed>[0-9]{3}\.[0-9]{2}),(?<direction>[0-9]{1,3}).*?#(?<day>[0-9]{2})(?<month>[0-9]{2})(?<year>[0-9]{2})#(?<time>[0-9]{6})', NULL, false);
INSERT INTO t_pattern (fk_id, fv_pattern, fv_description, fv_is_metric) VALUES (3, '[0-9]+,[^,]+,(?<source>\w{2})\w{3},(?<time>[0-9]{6})\.[^,]+,\w,(?<lat>[0-9]{3,5}\.[0-9]{1,4}),\w,(?<lng>[0-9]{3,5}\.[0-9]{1,4}),\w,(?<speed>[0-9]{1,3}\.[0-9]{1,2}),(?<direction>[0-9]{1,3})\..*?,(?<day>[0-9]{2})(?<month>[0-9]{2})(?<year>[0-9]{2}).+imei:(?<imei>[0-9]{15})', NULL, false);


--
-- Data for Name: t_equipment_model; Type: TABLE DATA; Schema: public; Owner: easylook
--

INSERT INTO t_equipment_model (fk_id, fv_name, fv_tac, fk_pattern) VALUES (1, 'Model 1', '8681300107', 1);


--
-- Name: t_equipment_model_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_equipment_model_fk_id_seq', 1, true);


--
-- Name: t_pattern_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_pattern_fk_id_seq', 6, true);


--
-- Data for Name: t_rule_type; Type: TABLE DATA; Schema: public; Owner: easylook
--



--
-- Data for Name: t_tracker_group; Type: TABLE DATA; Schema: public; Owner: easylook
--

INSERT INTO t_tracker_group (fk_id, fv_name, fk_user, ft_created) VALUES (2, 'тестовая группа', 9, '2015-05-19 16:40:51.637909');


--
-- Data for Name: t_tracker_type; Type: TABLE DATA; Schema: public; Owner: easylook
--

INSERT INTO t_tracker_type (fk_id, fv_name, fv_code, fv_description) VALUES (1, 'телефон', 'phone', 'phone');


--
-- Data for Name: t_tracker_img; Type: TABLE DATA; Schema: public; Owner: easylook
--

INSERT INTO t_tracker_img (fk_id, fv_path, fk_tracker_type) VALUES (1, '/home/rem', 1);


--
-- Data for Name: t_tracker; Type: TABLE DATA; Schema: public; Owner: easylook
--

INSERT INTO t_tracker (fk_id, fv_name, fk_group, fk_img, fk_user, ft_created, fb_active, fk_type) VALUES (1, 'Test 1', 2, 1, 9, '2015-05-19 16:43:25.17204', true, 1);


--
-- Data for Name: t_rule; Type: TABLE DATA; Schema: public; Owner: easylook
--



--
-- Data for Name: t_rule_channel; Type: TABLE DATA; Schema: public; Owner: easylook
--



--
-- Name: t_rule_channel_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_rule_channel_fk_id_seq', 1, false);


--
-- Name: t_rule_channel_fk_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_rule_channel_fk_id_seq1', 1, false);


--
-- Name: t_rule_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_rule_fk_id_seq', 1, false);


--
-- Name: t_rule_type_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_rule_type_fk_id_seq', 1, false);


--
-- Data for Name: t_tariff; Type: TABLE DATA; Schema: public; Owner: easylook
--



--
-- Name: t_tariff_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_tariff_fk_id_seq', 1, false);


--
-- Data for Name: t_tariff_rule; Type: TABLE DATA; Schema: public; Owner: easylook
--



--
-- Name: t_tariff_rule_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_tariff_rule_fk_id_seq', 1, false);


--
-- Data for Name: t_tracker_data_h; Type: TABLE DATA; Schema: public; Owner: easylook
--

INSERT INTO t_tracker_data_h (fk_id, fk_tracker, ft_utc_srv, ft_utc_tr, fv_lat, fv_lng, fv_speed) VALUES (1, 1, '2015-05-19 18:26:42.008', '2001-12-13 09:24:59', 59.9838749999999976, 30.2434839999999987, 18.8728049999999996);
INSERT INTO t_tracker_data_h (fk_id, fk_tracker, ft_utc_srv, ft_utc_tr, fv_lat, fv_lng, fv_speed) VALUES (2, 1, '2015-05-19 18:29:24.051', '2001-12-13 09:24:59', 59.9838749999999976, 30.2434839999999987, 18.8728049999999996);
INSERT INTO t_tracker_data_h (fk_id, fk_tracker, ft_utc_srv, ft_utc_tr, fv_lat, fv_lng, fv_speed) VALUES (3, 1, '2015-05-19 18:37:41.561', '2001-12-13 09:24:59', 59.9838749999999976, 30.2434839999999987, 18.8728049999999996);
INSERT INTO t_tracker_data_h (fk_id, fk_tracker, ft_utc_srv, ft_utc_tr, fv_lat, fv_lng, fv_speed) VALUES (4, 1, '2015-05-19 18:39:43.512', '2001-12-13 09:24:59', 59.9838749999999976, 30.2434839999999987, 18.8728049999999996);
INSERT INTO t_tracker_data_h (fk_id, fk_tracker, ft_utc_srv, ft_utc_tr, fv_lat, fv_lng, fv_speed) VALUES (5, 1, '2015-05-19 18:40:13.506', '2001-12-13 09:24:59', 59.9838749999999976, 30.2434839999999987, 18.8728049999999996);


--
-- Name: t_tracker_data_h_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_tracker_data_h_fk_id_seq', 5, true);


--
-- Data for Name: t_tracker_param; Type: TABLE DATA; Schema: public; Owner: easylook
--



--
-- Data for Name: t_tracker_data_p; Type: TABLE DATA; Schema: public; Owner: easylook
--



--
-- Name: t_tracker_data_p_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_tracker_data_p_fk_id_seq', 1, false);


--
-- Data for Name: t_tracker_equipment; Type: TABLE DATA; Schema: public; Owner: easylook
--

INSERT INTO t_tracker_equipment (fk_id, fv_name, fk_model, fk_tracker, fv_imei, fv_phone_number, ft_created) VALUES (1, 'erwerwer', 1, 1, '868130010729826', '790957787', '2015-05-19 18:01:44.015799');


--
-- Name: t_tracker_equipment_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_tracker_equipment_fk_id_seq', 1, true);


--
-- Name: t_tracker_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_tracker_fk_id_seq', 1, true);


--
-- Name: t_tracker_group_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_tracker_group_fk_id_seq', 2, true);


--
-- Name: t_tracker_img_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_tracker_img_fk_id_seq', 1, true);


--
-- Name: t_tracker_param_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_tracker_param_fk_id_seq', 1, false);


--
-- Data for Name: t_tracker_status; Type: TABLE DATA; Schema: public; Owner: easylook
--

INSERT INTO t_tracker_status (fk_id, fv_name, fv_code, fv_description) VALUES (1, 'in_moion', 'in_motion', 'in_motion');
INSERT INTO t_tracker_status (fk_id, fv_name, fv_code, fv_description) VALUES (2, 'in_place', 'in_place', 'in_place');


--
-- Data for Name: t_tracker_status_data; Type: TABLE DATA; Schema: public; Owner: easylook
--

INSERT INTO t_tracker_status_data (fk_id, fk_tracker, ft_since, ft_until, fk_status) VALUES (4, 1, '2015-05-19 18:39:43.576', '2030-12-31 00:00:00', 2);


--
-- Name: t_tracker_status_data_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_tracker_status_data_fk_id_seq', 4, true);


--
-- Name: t_tracker_status_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_tracker_status_fk_id_seq', 2, true);


--
-- Name: t_tracker_type_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_tracker_type_fk_id_seq', 1, true);


--
-- Name: t_user_fk_id_seq; Type: SEQUENCE SET; Schema: public; Owner: easylook
--

SELECT pg_catalog.setval('t_user_fk_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--

