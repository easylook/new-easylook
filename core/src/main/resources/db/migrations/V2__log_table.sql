CREATE TABLE t_log
(
  fk_id bigserial NOT NULL,
  fv_log_message character varying NOT NULL,
  ft_log_time timestamp without time zone NOT NULL DEFAULT now(),
  fk_user integer NOT NULL,
  CONSTRAINT t_log_pk PRIMARY KEY (fk_id),
  CONSTRAINT t_log_user_fk FOREIGN KEY (fk_user)
      REFERENCES t_user (fk_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE t_log
  OWNER TO easylook;