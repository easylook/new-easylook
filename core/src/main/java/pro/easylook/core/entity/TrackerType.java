package pro.easylook.core.entity;

import javax.persistence.*;

/**
 * Created
 * by artem
 * on 5/6/15.
 */
@Entity(name = "TrackerType")
@Table(name = "t_tracker_type")
public class TrackerType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @Column(name = "fv_name")
    private String name;

    @Column(name = "fv_code")
    private String code;

    @Column(name = "fv_description")
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
