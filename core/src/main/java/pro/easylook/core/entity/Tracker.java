package pro.easylook.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created
 * by artem
 * on 5/6/15.
 */

@Entity(name = "Tracker")
@Table(name = "t_tracker")
public class Tracker {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @Column(name = "fv_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "fk_group")
    private Group group;

    @ManyToOne
    @JoinColumn(name = "fk_img")
    private TrackerImage trackerImage;

    @ManyToOne
    @JoinColumn(name = "fk_user")
    private User user;

    @Column(name = "ft_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "fb_active")
    private boolean active;

    @ManyToOne
    @JoinColumn(name = "fk_type")
    private TrackerType trackerType;

    @ManyToOne
    @JoinColumn(name = "fk_tracker_equipment")
    private TrackerEquipment trackerEquipment;

    @ManyToOne
    @JoinColumn(name = "fk_tracker_tariff")
    private Tariff tariff;

    @OneToMany(mappedBy = "tracker", fetch = FetchType.EAGER)
    private List<Rule> rules;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @JsonIgnore
    public TrackerImage getTrackerImage() {
        return trackerImage;
    }

    public void setTrackerImage(TrackerImage trackerImage) {
        this.trackerImage = trackerImage;
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @JsonIgnore
    public TrackerType getTrackerType() {
        return trackerType;
    }

    public void setTrackerType(TrackerType trackerType) {
        this.trackerType = trackerType;
    }

    @JsonIgnore
    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    @JsonIgnore
    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    @JsonIgnore
    public TrackerEquipment getTrackerEquipment() {
        return trackerEquipment;
    }

    public void setTrackerEquipment(TrackerEquipment trackerEquipment) {
        this.trackerEquipment = trackerEquipment;
    }
}
