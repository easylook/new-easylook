package pro.easylook.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * Created
 * by artem
 * on 5/6/15.
 */
@Entity(name = "TrackerStatusData")
@Table(name = "t_tracker_status_data")
public class TrackerStatusData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "fk_tracker")
    private Tracker tracker;

    @ManyToOne
    @JoinColumn(name = "fk_status")
    private TrackerStatus trackerStatus;

    @Column(name = "ft_since")
    @Temporal(TemporalType.TIMESTAMP)
    private Date since;

    @Column(name = "ft_until")
    @Temporal(TemporalType.TIMESTAMP)
    private Date until;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonIgnore
    public Tracker getTracker() {
        return tracker;
    }

    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }

    @JsonIgnore
    public TrackerStatus getTrackerStatus() {
        return trackerStatus;
    }

    public void setTrackerStatus(TrackerStatus trackerStatus) {
        this.trackerStatus = trackerStatus;
    }

    public Date getSince() {
        return since;
    }

    public void setSince(Date since) {
        this.since = since;
    }

    public Date getUntil() {
        return until;
    }

    public void setUntil(Date until) {
        this.until = until;
    }
}

