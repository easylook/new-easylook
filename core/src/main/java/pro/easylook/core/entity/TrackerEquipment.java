package pro.easylook.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * Created
 * by artem
 * on 5/6/15.
 */

@Entity(name = "TrackerEquipment")
@Table(name = "t_tracker_equipment")
public class TrackerEquipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @Column(name = "fv_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "fk_model")
    private EquipmentModel model;

    @ManyToOne
    @JoinColumn(name = "fk_tracker")
    private Tracker tracker;

    @Column(name = "fv_imei")
    private String imei;

    @Column(name = "fv_sn")
    private String sn;

    @Column(name = "fv_phone_number")
    private String phoneNumber;

    @Column(name = "ft_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public EquipmentModel getModel() {
        return model;
    }

    public void setModel(EquipmentModel model) {
        this.model = model;
    }

    @JsonIgnore
    public Tracker getTracker() {
        return tracker;
    }

    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }
}
