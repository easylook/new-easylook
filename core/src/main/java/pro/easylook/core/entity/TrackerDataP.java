package pro.easylook.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created
 * by artem
 * on 5/6/15.
 */
@Entity(name = "TrackerDataP")
@Table(name = "t_tracker_data_p")
public class TrackerDataP {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "fk_header")
    private TrackerDataH trackerDataH;

    @ManyToOne
    @JoinColumn(name = "fk_param")
    private TrackerParam trackerParam;

    @Column(name = "fv_value")
    private String value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonIgnore
    public TrackerDataH getTrackerDataH() {
        return trackerDataH;
    }

    public void setTrackerDataH(TrackerDataH trackerDataH) {
        this.trackerDataH = trackerDataH;
    }

    public TrackerParam getTrackerParam() {
        return trackerParam;
    }

    public void setTrackerParam(TrackerParam trackerParam) {
        this.trackerParam = trackerParam;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
