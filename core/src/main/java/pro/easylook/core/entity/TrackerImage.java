package pro.easylook.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created
 * by artem
 * on 5/6/15.
 */
@Entity(name = "TrackerImage")
@Table(name = "t_tracker_img")
public class TrackerImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @Column(name = "fv_path")
    private String link;

    @ManyToOne
    @JoinColumn(name = "fk_tracker_type")
    private TrackerType trackerType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String path) {
        this.link = path;
    }

    @JsonIgnore
    public TrackerType getTrackerType() {
        return trackerType;
    }

    public void setTrackerType(TrackerType trackerType) {
        this.trackerType = trackerType;
    }
}
