package pro.easylook.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created
 * by artem
 * on 5/6/15.
 */
@Entity(name = "TrackerDataH")
@Table(name = "t_tracker_data_h")
public class TrackerDataH {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "fk_tracker")
    private Tracker tracker;

    @Column(name = "ft_utc_srv")
    @Temporal(TemporalType.TIMESTAMP)
    private Date serverTime;

    @Column(name = "ft_utc_tr")
    @Temporal(TemporalType.TIMESTAMP)
    private Date trackerTime;

    @Column(name = "fv_lat")
    private float lat;

    @Column(name = "fv_lng")
    private float lng;

    @Column(name = "fv_speed")
    private float speed;

    @Column(name = "fv_status_code")
    private String statusCode = TrackerStatus.CODE_IN_PLACE;

    @OneToMany(mappedBy = "trackerDataH", fetch = FetchType.EAGER)
    private List<TrackerDataP> trackerDataParams;

    @Column(name = "fv_direction")
    private int direction;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonIgnore
    public Tracker getTracker() {
        return tracker;
    }

    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }

    public Date getServerTime() {
        return serverTime;
    }

    public void setServerTime(Date serverTime) {
        this.serverTime = serverTime;
    }

    public Date getTrackerTime() {
        return trackerTime;
    }

    public void setTrackerTime(Date trackerTime) {
        this.trackerTime = trackerTime;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public double[] getPoint() {
        return new double[]{lat, lng};
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @JsonIgnore
    public List<TrackerDataP> getTrackerDataParams() {
        return trackerDataParams;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }
}
