package pro.easylook.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created
 * by artem
 * on 5/6/15.
 */

@Entity(name = "EquipmentModel")
@Table(name = "t_equipment_model")
public class EquipmentModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @Column(name = "fv_name")
    private String  name;

    @Column(name = "fv_tac")
    private String tac;

    @ManyToOne
    @JoinColumn(name = "fk_protocol")
    private Protocol protocol;

    @OneToMany(mappedBy = "equipmentModel", fetch = FetchType.EAGER)
    private List<TrackerParam> trackerParams;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTac() {
        return tac;
    }

    public void setTac(String tac) {
        this.tac = tac;
    }

    @JsonIgnore
    public List<TrackerParam> getTrackerParams() {
        return trackerParams;
    }

    public void setTrackerParams(List<TrackerParam> trackerParams) {
        this.trackerParams = trackerParams;
    }

    @JsonIgnore
    public Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    public Map<String, TrackerParam> getTrackerParamsMap() {
        final Map<String, TrackerParam> map = new HashMap<>();
        Lists.newArrayList(trackerParams)
                .parallelStream()
                .forEach((param -> map.put(param.getCode(), param)));

        return map;
    }
}
