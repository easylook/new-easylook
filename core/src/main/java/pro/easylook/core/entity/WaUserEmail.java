package pro.easylook.core.entity;

import javax.persistence.*;

@Entity(name = "WaUserEmail")
@Table(name = "wa_user_email")
public class WaUserEmail {

    @Id
    private long id;

    @ManyToOne
    @JoinColumn(name = "contact_id")
    private WaUser waUser;

    private String email;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public WaUser getWaUser() {
        return waUser;
    }

    public void setWaUser(WaUser waUser) {
        this.waUser = waUser;
    }
}
