package pro.easylook.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.Type;

import javax.persistence.*;

/**
 * Created
 * by artem
 * on 5/2/15.
 */
@Entity(name = "Area")
@Table(name = "t_area")
public class Area {

    public static final int TYPE_CIRCLE = 2;
    public static final int TYPE_POLYGON = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @Column(name = "fv_name")
    private String name;

    @Column(name = "fv_type")
    private int type;

    @Column(name = "fv_shape", columnDefinition="Geometry", nullable = true)
    @Type(type = "org.hibernate.spatial.GeometryType")
    private Geometry shape;

    @ManyToOne
    @JoinColumn(name = "fk_user")
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Geometry getShape() {
        return shape;
    }

    public void setShape(Geometry shape) {
        this.shape = shape;
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
