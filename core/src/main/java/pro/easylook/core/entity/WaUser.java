package pro.easylook.core.entity;

import javax.persistence.*;
import java.util.List;

@Entity(name = "WaUser")
@Table(name = "wa_user")
public class WaUser {

    @Id
    private long id;

    private String name;

    private String login;

    private String firstname;

    private String middlename;

    private String lastname;

    private String password;

    @OneToMany(mappedBy = "waUser", fetch = FetchType.EAGER)
    private List<WaUserEmail> emails;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List<WaUserEmail> getEmails() {
        return emails;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
