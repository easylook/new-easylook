package pro.easylook.core.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created
 * by artem
 * on 6/3/15.
 */
@Entity(name = "Log")
@Table(name = "t_log")
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private long id;

    @Column(name = "fv_log_message")
    private String message;

    @Column(name = "ft_log_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date logTime = new Date();

    @ManyToOne
    @JoinColumn(name = "fk_user")
    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getLogTime() {
        return logTime;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
