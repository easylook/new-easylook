package pro.easylook.core.entity;

import javax.persistence.*;

/**
 * Created
 * by artem
 * on 6/28/15.
 */
@Entity(name = "Template")
@Table(name = "t_template")
public class Template {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @Column(name = "fv_pattern")
    private String pattern;

    @ManyToOne
    @JoinColumn(name = "fk_model")
    private EquipmentModel model;

    @ManyToOne
    @JoinColumn(name = "fk_protocol")
    private Protocol protocol;

    @Column(name = "fv_tag")
    private String tag;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public EquipmentModel getModel() {
        return model;
    }

    public void setModel(EquipmentModel model) {
        this.model = model;
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
