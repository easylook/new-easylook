package pro.easylook.core.entity;

import javax.persistence.*;

/**
 * Created
 * by artem
 * on 6/26/15.
 */
@Entity(name = "Protocol")
@Table(name = "t_protocol")
public class Protocol {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @Column(name = "fv_name")
    private String name;

    @Column(name = "fv_port")
    private int port;

    @Column(name = "fv_imei_offset")
    private int imeiOffset;

    @Column(name = "fv_imei_count")
    private int imeiCount;

    @Column(name = "fv_parser_class_name")
    private String parserClassName;

    @Column(name = "fv_id_regexp")
    private String idRegexp;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPort() {
        return port;
    }

    public int getImeiOffset() {
        return imeiOffset;
    }

    public int getImeiCount() {
        return imeiCount;
    }

    public boolean isFullImei() {
        return imeiCount == 15;
    }

    public String getParserClassName() {
        return parserClassName;
    }

    public String getIdRegexp() {
        return idRegexp;
    }
}
