package pro.easylook.core.entity;

import javax.persistence.*;

/**
 * Created
 * by artem
 * on 5/2/15.
 */
@Entity(name = "User")
@Table(name = "t_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @Column(name = "fv_fname")
    private String fName;

    @Column(name = "fv_mname")
    private String mName;

    @Column(name = "fv_sname")
    private String sName;

    @Column(name = "fv_login")
    private String login;

    @Column(name = "fv_psw")
    private String password;

    @Column(name = "fv_email")
    private String email;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
