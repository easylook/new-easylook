package pro.easylook.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

/**
 * Created
 * by artem
 * on 5/6/15.
 */

@Entity(name = "Rule")
@Table(name = "t_rule")
public class Rule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @Column(name = "fv_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "fk_tracker")
    private Tracker tracker;

    @ManyToOne
    @JoinColumn(name = "fk_zone")
    private Area area;

    @ManyToOne
    @JoinColumn(name = "fk_rule_type")
    private RuleType ruleType;

    @ManyToOne
    @JoinColumn(name = "fk_user")
    private User user;

    @ManyToMany
    @JoinTable(
            name="t_rule_channel",
            joinColumns={@JoinColumn(name="fk_rule", referencedColumnName="fk_id")},
            inverseJoinColumns={@JoinColumn(name="fk_channel", referencedColumnName="fk_id")})
    private List<Channel> channels;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public Tracker getTracker() {
        return tracker;
    }

    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }

    @JsonIgnore
    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    @JsonIgnore
    public RuleType getRuleType() {
        return ruleType;
    }

    public void setRuleType(RuleType ruleType) {
        this.ruleType = ruleType;
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @JsonIgnore
    public List<Channel> getChannels() {
        return channels;
    }

    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }
}
