package pro.easylook.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created
 * by artem
 * on 5/6/15.
 */

@Entity(name = "Group")
@Table(name = "t_tracker_group")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fk_id")
    private int id;

    @Column(name = "fv_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "fk_user")
    private User user;

    @Column(name = "ft_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @ManyToOne
    @JoinColumn(name = "fk_tracker_image")
    private TrackerImage image;

    @OneToMany(mappedBy = "group", fetch = FetchType.EAGER)
    private List<Tracker> trackers;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<Tracker> getTrackers() {
        return trackers;
    }

    public void setTrackers(List<Tracker> trackers) {
        this.trackers = trackers;
    }

    @JsonIgnore
    public TrackerImage getImage() {
        return image;
    }

    public void setImage(TrackerImage image) {
        this.image = image;
    }
}