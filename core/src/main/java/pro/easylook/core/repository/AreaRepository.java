package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.Area;
import pro.easylook.core.entity.User;

import java.util.List;

public interface AreaRepository extends CrudRepository<Area, Integer> {
    List<Area> findByUser(User user);
    Area findByIdAndUser(Integer id, User user);
}
