package pro.easylook.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pro.easylook.core.entity.Tracker;
import pro.easylook.core.entity.TrackerStatusData;

import java.util.List;

public interface TrackerStatusDataRepository extends CrudRepository<TrackerStatusData, Integer> {

    @Query("SELECT tsd FROM TrackerStatusData tsd WHERE tsd.tracker = :tracker AND tsd.until = '2030-12-31 00:00:00'")
    TrackerStatusData findLatestTrackerStatusData(@Param("tracker") Tracker tracker);

    @Query("SELECT tsd FROM TrackerStatusData tsd WHERE tsd.tracker in (:trackers) AND tsd.until > current_timestamp")
    List<TrackerStatusData> findLatestTrackersStatusData(@Param("trackers") List<Tracker> trackers);
}
