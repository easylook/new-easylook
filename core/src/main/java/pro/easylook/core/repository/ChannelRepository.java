package pro.easylook.core.repository;


import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.Channel;

public interface ChannelRepository extends CrudRepository<Channel, Integer> {
}
