package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.TrackerStatus;

public interface TrackerStatusRepository extends CrudRepository<TrackerStatus, Integer> {
}
