package pro.easylook.core.repository;


import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.EquipmentModel;
import pro.easylook.core.entity.Tariff;
import pro.easylook.core.entity.Template;

public interface TemplateRepository extends CrudRepository<Template, Integer> {

    Template findOneByTagAndModel(String tag, EquipmentModel equipmentModel);
}
