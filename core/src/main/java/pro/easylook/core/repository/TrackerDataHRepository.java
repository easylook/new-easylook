package pro.easylook.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pro.easylook.core.entity.Tracker;
import pro.easylook.core.entity.TrackerDataH;

import java.util.Date;
import java.util.List;

/**
 * Created
 * by artem
 * on 5/13/15.
 */
public interface TrackerDataHRepository extends CrudRepository<TrackerDataH, Integer> {

    @Query("SELECT t from TrackerDataH t WHERE t.tracker IN (:trackers) and t.serverTime IN (SELECT MAX(t1.serverTime) FROM TrackerDataH t1 where t.tracker = t1.tracker)")
    List<TrackerDataH> findLatestTrackerPoints(@Param("trackers") List<Tracker> trackers);
    List<TrackerDataH> findByTrackerAndServerTimeBetween(Tracker tracker, Date fromDate, Date toDate);
}
