package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.TrackerType;

public interface TrackerTypeRepository extends CrudRepository<TrackerType, Integer> {
}
