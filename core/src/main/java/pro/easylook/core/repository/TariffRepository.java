package pro.easylook.core.repository;


import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.Tariff;

public interface TariffRepository extends CrudRepository<Tariff, Integer> {
}
