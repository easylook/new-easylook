package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.Tracker;
import pro.easylook.core.entity.User;

import java.util.List;


public interface TrackerRepository extends CrudRepository<Tracker, Integer> {
    List<Tracker> findByUser(User user);
    Tracker findByIdAndUser(Integer id, User user);
}
