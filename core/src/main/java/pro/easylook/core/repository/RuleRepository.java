package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.Rule;
import pro.easylook.core.entity.User;

import java.util.List;

public interface RuleRepository extends CrudRepository<Rule, Integer> {
    List<Rule> findByUser(User user);
    Rule findByIdAndUser(Integer id, User user);
}
