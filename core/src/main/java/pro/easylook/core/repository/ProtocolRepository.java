package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.Log;
import pro.easylook.core.entity.Protocol;
import pro.easylook.core.entity.User;

import java.util.List;

public interface ProtocolRepository extends CrudRepository<Protocol, Integer> {
}
