package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.Group;
import pro.easylook.core.entity.User;

import java.util.List;

public interface GroupRepository extends CrudRepository<Group, Integer> {
    List<Group> findByUser(User user);
    Group findByIdAndUser(Integer id, User user);
}
