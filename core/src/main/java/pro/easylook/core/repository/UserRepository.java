package pro.easylook.core.repository;


import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.User;

/**
 * @author aevseenko
 * @since 3/31/15
 */
public interface UserRepository extends CrudRepository<User, Integer> {
}
