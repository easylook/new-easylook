package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.WaUserEmail;

/**
 * @author aevseenko
 * @since 2/26/15
 */
public interface WaUserEmailRepository extends CrudRepository<WaUserEmail, Integer> {
}
