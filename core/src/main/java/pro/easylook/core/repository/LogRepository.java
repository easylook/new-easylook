package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.Area;
import pro.easylook.core.entity.Log;
import pro.easylook.core.entity.User;

import java.util.List;

public interface LogRepository extends CrudRepository<Log, Long> {
    List<Log> findByUser(User user);
}
