package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.EquipmentModel;

/**
 * Created
 * by artem
 * on 5/21/15.
 */
public interface EquipmentModelRepository extends CrudRepository<EquipmentModel, Integer> {
}
