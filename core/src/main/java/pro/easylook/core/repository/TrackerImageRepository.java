package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.TrackerImage;

public interface TrackerImageRepository extends CrudRepository<TrackerImage, Integer> {
}
