package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.WaUser;

/**
 * @author aevseenko
 * @since 2/26/15
 */
public interface WaUserRepository extends CrudRepository<WaUser, Integer> {
}
