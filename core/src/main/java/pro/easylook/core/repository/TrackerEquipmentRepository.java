package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.TrackerEquipment;

/**
 * Created
 * by artem
 * on 5/12/15.
 */
public interface TrackerEquipmentRepository extends CrudRepository<TrackerEquipment, Integer> {
    TrackerEquipment findOneByImei(String imei);

    TrackerEquipment findOneBySn(String sn);
}
