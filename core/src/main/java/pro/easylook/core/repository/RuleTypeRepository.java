package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.RuleType;

public interface RuleTypeRepository extends CrudRepository<RuleType, Integer> {
}
