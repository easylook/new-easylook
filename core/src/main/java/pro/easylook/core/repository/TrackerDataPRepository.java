package pro.easylook.core.repository;

import org.springframework.data.repository.CrudRepository;
import pro.easylook.core.entity.TrackerDataP;

/**
 * Created
 * by artem
 * on 5/13/15.
 */
public interface TrackerDataPRepository extends CrudRepository<TrackerDataP, Integer> {
}
